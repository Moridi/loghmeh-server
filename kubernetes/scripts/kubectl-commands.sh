##### MySQL Server #####

kubectl delete deployment mysql
kubectl delete service mysql

kubectl delete pvc mysql-pv-claim

kubectl apply -f ../config/mysql/mysql-pv-claim.yaml

kubectl apply -f ../config/mysql/mysql-deployment.yaml
kubectl apply -f ../config/mysql/mysql-service.yaml

##### Loghmeh #####

kubectl delete service loghmeh
kubectl delete deployment loghmeh

kubectl apply -f ../config/loghmeh/loghmeh-deployment.yaml
kubectl apply -f ../config/loghmeh/loghmeh-service.yaml

kubectl set image deployments/loghmeh loghmeh=docker.io/s3reuis/loghmeh-back-end:latest
kubectl scale deploy loghmeh --replicas=2

##### Description #####

kubectl describe services mysql
kubectl describe deployment mysql

kubectl describe pvc mysql-pv-claim

kubectl describe deployment loghmeh
kubectl describe services loghmeh

kubectl get pod
kubectl get services

kubectl describe quota
kubectl get pod -o wide

##### MySQL Client #####

kubectl delete pod mysql-client
kubectl apply -f ../config/mysql/mysql-client.yaml

kubectl exec -it mysql-client /bin/ash
mysql -h mysql -u root -prootroot