drop database if exists loghmeh;
create database loghmeh character set UTF8 collate utf8_bin;

use loghmeh;

create table user
(
    username varchar(30) not null,
    firstName char(20) null,
    lastName char(20) null,
    phone char(20) null,
    email varchar(30) not null,
    balance int default 0 not null,
    userLocationX mediumint default 0 not null,
    userLocationY mediumint default 0 not null,
    password char(128) not null,
    constraint user_pk
        primary key (username)
);

create table restaurant
(
    restaurantId char(30) not null,
    name char(50) not null,
    logo varchar(1000) null,
    restaurantLocationX mediumint default 0 not null,
    restaurantLocationY mediumint default 0 not null,
    constraint restaurant_pk
        primary key (restaurantId)
);

create table food
(
    foodId int auto_increment,
    name varchar(100) not null,
    description varchar(500) null,
    popularity double default 0 not null,
    price int default 0 not null,
    image varchar(1000) null,
    constraint food_pk
        primary key (foodId)
);

create table partyFood
(
    foodId int not null,
    oldPrice int not null,
    count smallint default 1 not null,
    expired boolean default false,

    constraint partyFood_pk
        primary key (foodId),

    constraint partyFood_food_fk
        foreign key (foodId)
            references food (foodId)
            on delete cascade
);

create table menuItem
(
    restaurantId char(30) not null,
    foodId int not null,

    constraint menu_pk
        primary key (restaurantId, foodId),

    constraint menu_restaurant_fk
        foreign key (restaurantId)
            references restaurant (restaurantId)
            on delete cascade,

    constraint menu_food_fk
        foreign key (foodId)
            references food (foodId)
            on delete cascade
);

create table userOrder
(
    userOrderId int auto_increment,
    username varchar(30) not null,
    restaurantId char(30) not null,
    state varchar(30) default 'FINDING_DELIVERY' not null,

    constraint order_pk
        primary key (userOrderId),

    constraint order_restaurant_fk
        foreign key (restaurantId)
            references restaurant (restaurantId)
            on delete cascade,

    constraint order_user_fk
        foreign key (username)
            references user (username)
            on delete cascade
);

create table delivery
(
    deliveryId char(50) not null,
    velocity int null,
    deliveryLocationX mediumint default 0 not null,
    deliveryLocationY mediumint default 0 not null,
    deliveryDuration double null,

    userOrderId int not null,

    constraint deliveryId
        primary key (deliveryId),

    constraint delivery_order_fk
        foreign key (userOrderId)
            references userOrder (userOrderId)
            on delete cascade
);

create table orderItem
(
    userOrderId int not null,
    foodId int not null,
    count smallint not null,

    constraint order_pk
        primary key (userOrderId, foodId),

    constraint orderItem_food_fk
        foreign key (foodId)
            references food (foodId)
            on delete cascade,

    constraint orderItem_order_fk
        foreign key (userOrderId)
            references userOrder (userOrderId)
            on delete cascade
);
