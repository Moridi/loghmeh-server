package ir.loghmeh.controllers;

import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ir.loghmeh.exceptions.restaurant.DistantRestaurantException;
import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.restaurant.dto.RestaurantSummaryDTO;
import ir.loghmeh.services.ApplicationService;
import ir.loghmeh.services.implementation.ApplicationServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import java.util.List;

@Api(value = "RestaurantController")
@RestController
public class RestaurantController {

    private final static ApplicationService applicationService =
            ApplicationServiceImpl.getInstance();

    // ############################ Get all the restaurants ############################

    @ApiOperation(
            value = "Get all the near restaurants",
            response = Iterable.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation")
    })

    @RequestMapping(value = "/restaurant", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<List<RestaurantSummaryDTO>> getRestaurants(
            @RequestParam(name="startAt") int startAt,
            @RequestAttribute Claims claims) {

        String username = claims.getId();
        return ResponseEntity.ok(applicationService.
                getNearRestaurantsDTO(username, startAt));
    }

    // ############################ Get specific restaurant ############################

    @ApiOperation(
            value = "Get specific restaurant information",
            response = Restaurant.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 404,
                    message = "Requested restaurant has not found",
                    response = DistantRestaurantException.class),
            @ApiResponse(code = 400,
                    message = "Requested restaurant is so far from user",
                    response = RestaurantNotFoundException.class)
    })

    @RequestMapping(path = "/restaurant/{restaurantId}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity getSpecificRestaurant(
            @PathVariable(value = "restaurantId") String restaurantId,
            @RequestAttribute Claims claims) {

        String username = claims.getId();

        try {
            return ResponseEntity.ok(applicationService.getNearRestaurantDTOById(
                    username, restaurantId));
        } catch (DistantRestaurantException exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    exception.getMessage());
        } catch (RestaurantNotFoundException exception) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    exception.getMessage());
        }
    }

    // ############################ Search restaurants ############################

    @ApiOperation(
            value = "Search restaurants by name or foodName",
            response = Iterable.class,
            responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation")
    })
    @RequestMapping(path = "/search", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<List<RestaurantSummaryDTO>> searchRestaurants(
            @RequestParam(value = "restaurantName") String restaurantName,
            @RequestParam(value = "foodName") String foodName,
            @RequestParam(name="startAt") int startAt,
            @RequestAttribute Claims claims) {

        String username = claims.getId();

        return ResponseEntity.ok(
                applicationService.searchRestaurants(restaurantName, foodName, username, startAt));
    }
}