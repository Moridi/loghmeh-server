package ir.loghmeh.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ir.loghmeh.exceptions.authentication.InvalidToken;
import ir.loghmeh.exceptions.authentication.UserAlreadyExists;
import ir.loghmeh.exceptions.user.InvalidUserData;
import ir.loghmeh.models.user.User;
import ir.loghmeh.services.ApplicationService;
import ir.loghmeh.services.AuthenticationService;
import ir.loghmeh.services.implementation.ApplicationServiceImpl;
import ir.loghmeh.services.implementation.AuthenticationServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.security.crypto.bcrypt.BCrypt;

@Api(value = "AuthenticationController")
@RestController
public class AuthenticationController {

    private final static ApplicationService applicationService =
            ApplicationServiceImpl.getInstance();

    private final static AuthenticationService authenticationService =
            AuthenticationServiceImpl.getInstance();

    // ############################ Sign-up ############################

    @ApiOperation(
            value = "User Sign-up")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 403,
                    message = "Requested user already exists!",
                    response = UserAlreadyExists.class),
            @ApiResponse(code = 403,
                    message = "Sign-up has failed!",
                    response = UserAlreadyExists.class)
    })

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity signup(@RequestParam("firstName") String firstName,
                                 @RequestParam("lastName") String lastName,
                                 @RequestParam("phone") String phone,
                                 @RequestParam("username") String username,
                                 @RequestParam("password") String password) {

        if (!applicationService.doesUserExist(username)) {
            applicationService.registerUser(firstName, lastName, phone,
                                            username, password);
            String token = authenticationService.createJWT(username);
            return ResponseEntity.ok(token);

        } else return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                new UserAlreadyExists().getMessage());
    }

    // ############################ Login ############################

    @ApiOperation(
            value = "User Login")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 403,
                    message = "Invalid username or password!",
                    response = UserAlreadyExists.class)
    })

    @RequestMapping(value = "/login", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity login(@RequestParam("username") String username,
                                @RequestParam("password") String password) {

        if (!applicationService.doesUserExist(username))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    new InvalidUserData().getMessage());

        User user = applicationService.getUser(username);

        if (!BCrypt.checkpw(password, user.getPassword()))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    new InvalidUserData().getMessage());

        String token = authenticationService.createJWT(username);

        return ResponseEntity.ok(token);
    }

    // ######################### Google Auth #########################

    @ApiOperation(
            value = "User Login with Google API")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 403,
                    message = "Invalid username or password!",
                    response = UserAlreadyExists.class)
    })

    @RequestMapping(value = "/login/google", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity loginWithGoogle(@RequestParam("token") String token) {
        try {
            String username = authenticationService.decodeGoogleIdToken(token);

            if (!applicationService.doesUserExist(username))
                return ResponseEntity.ok("");

            String jwtToken = authenticationService.createJWT(username);
            return ResponseEntity.ok(jwtToken);
        } catch (InvalidToken invalidToken) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    invalidToken.getMessage());
        }
    }
}