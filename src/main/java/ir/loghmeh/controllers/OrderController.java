package ir.loghmeh.controllers;

import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ir.loghmeh.exceptions.order.NoSuchOrderException;
import ir.loghmeh.models.order.dto.OrderDTO;
import ir.loghmeh.services.ApplicationService;
import ir.loghmeh.services.implementation.ApplicationServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "OrderController")
@RestController
public class OrderController {
    private final static ApplicationService applicationService =
            new ApplicationServiceImpl().getInstance();

    // ############################ Get all the orders ############################

    @ApiOperation(
            value = "Get all the orders",
            response = Iterable.class,
            responseContainer = "Set")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation")
    })

    @RequestMapping(value = "/order", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<List<OrderDTO>> getOrders(@RequestAttribute Claims claims) {

        String username = claims.getId();

        List<OrderDTO> orders = applicationService.getOrdersDTO(username);
        return ResponseEntity.ok(orders);
    }

    // ############################ Get Order by ID ############################

    @ApiOperation(
            value = "Get specific order",
            response = OrderDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 400,
                    message = "Requested order is not found",
                    response = NoSuchOrderException.class)
    })

    @RequestMapping(value = "/order/{orderId}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity getOrder(
            @PathVariable(value = "orderId") String orderId,
            @RequestAttribute Claims claims) {

        String username = claims.getId();

        try {
            OrderDTO order = applicationService.getOrderDTOById(username, orderId);
            return ResponseEntity.ok(order);
        } catch (NoSuchOrderException exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    exception.getMessage());
        }
    }
}
