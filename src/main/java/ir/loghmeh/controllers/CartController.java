package ir.loghmeh.controllers;

import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ir.loghmeh.exceptions.cart.EmptyCartException;
import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.foodParty.PartyFoodExpired;
import ir.loghmeh.exceptions.foodParty.PartyFoodSoldOut;
import ir.loghmeh.exceptions.restaurant.DistantRestaurantException;
import ir.loghmeh.exceptions.restaurant.ImproperRestaurantException;
import ir.loghmeh.exceptions.user.NoCreditException;
import ir.loghmeh.models.cart.dto.CartDTO;
import ir.loghmeh.models.cart.dto.CartItemDTO;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.services.ApplicationService;
import ir.loghmeh.services.implementation.ApplicationServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "CartController")
@RestController
public class CartController {
    private final static ApplicationService applicationService =
            new ApplicationServiceImpl().getInstance();

    // ############################ Get User Cart ############################

    @ApiOperation(
            value = "Get specific user cart",
            response = CartDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation")
    })

    @RequestMapping(value = "/cart", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<CartDTO> getUserCart(@RequestAttribute Claims claims) {

        String username = claims.getId();

        return ResponseEntity.ok(applicationService.getUserCartDTO(username));
    }

    // ############################ Add to Cart ############################

    @Deprecated
    @ApiOperation(
            value = "Add food to the user cart")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 400,
                    message = "Requested food has not found",
                    response = FoodNotFoundException.class),
            @ApiResponse(code = 400,
                    message = "Different restaurants in the same cart is not allowed",
                    response = ImproperRestaurantException.class),
            @ApiResponse(code = 400,
                    message = "Requested restaurant is so far from user",
                    response = DistantRestaurantException.class)
    })

    @RequestMapping(path = "/cart",
            params = { "restaurantId", "foodId" },
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity addToCart(
            @RequestParam(name = "foodId") String foodId,
            @RequestAttribute Claims claims) {

        String username = claims.getId();

        try {
            applicationService.addToCart(username, foodId);
            return ResponseEntity.ok("Successful");
        } catch (FoodNotFoundException | ImproperRestaurantException
                    | DistantRestaurantException exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    exception.getMessage());
        }
    }

    // ############################ Remove Food from Cart ############################

    @Deprecated
    @ApiOperation(
            value = "Remove food from user cart")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 400,
                    message = "Requested food has not found",
                    response = FoodNotFoundException.class)
    })

    @RequestMapping(path = "/cart",
            params = { "restaurantId", "foodId" },
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity removeFromCart(
            @RequestParam(name = "foodId") String foodId,
            @RequestAttribute Claims claims) {

        String username = claims.getId();

        try {
            applicationService.removeFromCart(username, foodId);
            return ResponseEntity.ok("Successful");
        } catch (FoodNotFoundException exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    exception.getMessage());
        }
    }

    // ############################ Finalize order ############################

    @ApiOperation(
            value = "Finalize last order of user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 400,
                    message = "User cart is empty",
                    response = EmptyCartException.class),
            @ApiResponse(code = 400,
                    message = "User doesn't have enough credit",
                    response = NoCreditException.class),
            @ApiResponse(code = 400,
                    message = "Requested party food has been expired",
                    response = PartyFoodExpired.class),
            @ApiResponse(code = 400,
                    message = "Requested party food has been sold",
                    response = PartyFoodSoldOut.class),
            @ApiResponse(code = 400,
                    message = "Requested food doesn't exist",
                    response = FoodNotFoundException.class)
    })

    @RequestMapping(path = "/cart/finalizeOrder",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity finalizeOrder(
            @RequestBody List<CartItemDTO> cartDTO,
            @RequestAttribute Claims claims) {

        String username = claims.getId();

        try {
            applicationService.initializeCart(username, cartDTO);
            Order order = applicationService.finalizeOrder(username);
            return ResponseEntity.ok("Successful");
        } catch (EmptyCartException
                | NoCreditException
                | PartyFoodExpired
                | PartyFoodSoldOut
                | FoodNotFoundException
                | ImproperRestaurantException
                | DistantRestaurantException exception) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    exception.getMessage());
        }
    }
}