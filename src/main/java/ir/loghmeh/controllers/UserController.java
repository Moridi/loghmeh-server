package ir.loghmeh.controllers;

import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ir.loghmeh.exceptions.user.InvalidUserData;
import ir.loghmeh.models.user.User;
import ir.loghmeh.models.user.dto.UserDTO;
import ir.loghmeh.services.ApplicationService;
import ir.loghmeh.services.implementation.ApplicationServiceImpl;
import ir.loghmeh.utilities.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value = "UserController")
@RestController
public class UserController {
    private final static ApplicationService applicationService =
            ApplicationServiceImpl.getInstance();

    // ############################ Get user information ############################

    @ApiOperation(
            value = "Get user information",
            response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 400,
                    message = "Invalid username",
                    response = InvalidUserData.class)
    })

    @RequestMapping(value = "/user", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity getUser(@RequestAttribute Claims claims) {
        String username = claims.getId();

        try {
            UserDTO user = applicationService.getUserDTO(username);
            return ResponseEntity.ok(user);

        } catch (InvalidUserData invalidUserData) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).
                    body(invalidUserData.getMessage());
        }
    }

    @ApiOperation(
            value = "Charge user account")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation"),
            @ApiResponse(code = 400,
                    message = "Invalid amount",
                    response = NumberFormatException.class)
    })

    @RequestMapping(path = "/user", params = { "amount" },
            method = RequestMethod.PUT)

    public ResponseEntity chargeUserAccount(
            @RequestParam(name = "amount") String amount,
            @RequestAttribute Claims claims) {

        String username = claims.getId();

        try {
            if (StringUtils.isPositive(amount))
            {
                applicationService.chargeUserAccount(
                        username, Integer.parseInt(amount));
                return ResponseEntity.ok("Successful");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    "Too large amount");
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                "Invalid input format!");
    }
}