package ir.loghmeh.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ir.loghmeh.models.foodParty.dto.FoodPartyDTO;
import ir.loghmeh.services.ApplicationService;
import ir.loghmeh.services.implementation.ApplicationServiceImpl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "FoodPartyController")
@RestController
public class FoodPartyController {
    private final static ApplicationService applicationService =
            new ApplicationServiceImpl().getInstance();

    @ApiOperation(
            value = "Get Food Party Foods",
            response = FoodPartyDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful operation")
    })

    @RequestMapping(value = "/foodParty", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<FoodPartyDTO> getPartyFoods() {
        return ResponseEntity.ok(applicationService.getFoodPartyDTO());
    }
}
