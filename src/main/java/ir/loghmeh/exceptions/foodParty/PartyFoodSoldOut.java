package ir.loghmeh.exceptions.foodParty;

public class PartyFoodSoldOut extends Exception {
    public PartyFoodSoldOut() {
        super("PARTY_FOOD_SOLD_OUT");
    }
}
