package ir.loghmeh.exceptions.foodParty;

public class PartyFoodExpired extends Exception {
    public PartyFoodExpired() {
        super("PARTY_FOOD_EXPIRED");
    }
}
