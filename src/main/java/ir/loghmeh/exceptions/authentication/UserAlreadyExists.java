package ir.loghmeh.exceptions.authentication;

public class UserAlreadyExists extends Exception {
    public UserAlreadyExists() {
        super("DUPLICATE_USER");
    }
}