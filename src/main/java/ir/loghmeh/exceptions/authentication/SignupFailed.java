package ir.loghmeh.exceptions.authentication;

public class SignupFailed extends Exception {
    public SignupFailed() {
        super("SIGNUP_FAILED");
    }
}