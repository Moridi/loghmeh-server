package ir.loghmeh.exceptions.authentication;

public class InvalidToken extends Exception {
    public InvalidToken() {
        super("INVALID_TOKEN");
    }
}