package ir.loghmeh.exceptions.user;

public class NoCreditException extends Exception {
    public NoCreditException() {
        super("NO_CREDIT");
    }
}
