package ir.loghmeh.exceptions.user;

public class InvalidUserData extends Exception {
    public InvalidUserData() {
        super("INVALID_USER_DATA");
    }
}
