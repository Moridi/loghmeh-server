package ir.loghmeh.exceptions.food;

public class FoodNotFoundException extends Exception {
    public FoodNotFoundException() {
        super("FOOD_NOT_FOUND");
    }
}
