package ir.loghmeh.exceptions.food;

public class DuplicateFoodException extends Exception {
    public DuplicateFoodException() {
        super("DUPLICATE_FOOD");
    }
}
