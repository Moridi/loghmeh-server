package ir.loghmeh.exceptions.cart;

public class EmptyCartException extends Exception {
    public EmptyCartException() {
        super("EMPTY_CART");
    }
}
