package ir.loghmeh.exceptions.restaurant;

public class DistantRestaurantException extends Exception {
    public DistantRestaurantException() {
        super("DISTANT_RESTAURANT");
    }
}
