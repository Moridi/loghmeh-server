package ir.loghmeh.exceptions.restaurant;

public class DuplicateRestaurantException extends Exception {
    public DuplicateRestaurantException() {
        super("DUPLICATE_RESTAURANT");
    }
}
