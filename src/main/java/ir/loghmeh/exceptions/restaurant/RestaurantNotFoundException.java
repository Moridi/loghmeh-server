package ir.loghmeh.exceptions.restaurant;

public class RestaurantNotFoundException extends Exception {
    public RestaurantNotFoundException() {
        super("RESTAURANT_NOT_FOUND");
    }
}
