package ir.loghmeh.exceptions.restaurant;

public class ImproperRestaurantException extends Exception {
    public ImproperRestaurantException() {
        super("IMPROPER_RESTAURANT");
    }
}
