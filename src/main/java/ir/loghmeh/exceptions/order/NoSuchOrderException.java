package ir.loghmeh.exceptions.order;

public class NoSuchOrderException extends Exception  {
    public NoSuchOrderException() {
            super("NO_SUCH_ORDER");
        }
}
