package ir.loghmeh.filters;

import io.jsonwebtoken.Claims;
import ir.loghmeh.exceptions.authentication.InvalidToken;
import ir.loghmeh.services.implementation.AuthenticationServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@WebFilter(filterName="AuthenticationFilter")
public class AuthenticationFilter implements Filter {

    private void invalidTokenException(ServletResponse response)
            throws IOException {
        HttpServletResponse res = (HttpServletResponse)response;
        res.setStatus(401);
        res.getWriter().write(new InvalidToken().getMessage());
    }

    private Boolean isNormalPage(HttpServletRequest req) {
        return req.getRequestURI().equals("/signup")
                || req.getRequestURI().equals("/login")
                || req.getRequestURI().equals("/login/google");
    }

    private void setClaims(ServletRequest request,
                           ServletResponse response,
                           FilterChain chain,
                           String token)
            throws IOException {

        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;

        try {
            Claims claims = AuthenticationServiceImpl.
                    getInstance().decodeJWT(token);
            req.setAttribute("claims", claims);
            chain.doFilter(request, response);
        } catch (Exception e) {
            System.out.println(e.getCause().toString());
            invalidTokenException(res);
        }
    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain)
            throws IOException,
            ServletException {

        HttpServletRequest req = (HttpServletRequest)request;

        if (isNormalPage(req))
        {
            chain.doFilter(request, response);
            return;
        }

        String token = req.getHeader("authorization");

        if (isEmpty(token)) {
            invalidTokenException(response);
            return;
        }

        setClaims(request, response, chain, token);
    }
}