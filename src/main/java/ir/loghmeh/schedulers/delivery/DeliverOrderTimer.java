package ir.loghmeh.schedulers.delivery;

import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.OrderState;
import ir.loghmeh.services.implementation.OrderServiceImpl;

import java.util.Timer;
import java.util.TimerTask;

public class DeliverOrderTimer extends TimerTask {
    private Order order;

    public static void startTimer(Order order) {
        Timer timer = new Timer();
        TimerTask task = new DeliverOrderTimer(order);

        long delay = (long) (order.getDeliveryInfo().getDeliveryDuration() * 1000);
        timer.schedule(task, delay);
    }

    public DeliverOrderTimer(Order order) {
        this.order = order;
    }

    @Override
    public void run() {
        OrderServiceImpl.getInstance().updateOrderState(this.order, OrderState.DELIVERED);
    }
}
