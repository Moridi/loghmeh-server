package ir.loghmeh.schedulers.delivery;

import ir.loghmeh.models.order.DeliveryInfo;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.OrderState;
import ir.loghmeh.services.implementation.DeliveryServiceImpl;
import ir.loghmeh.services.implementation.OrderServiceImpl;
import ir.loghmeh.utilities.constants.SchedulersConstants;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class FindDeliveryScheduler implements Runnable {
    private Order order;
    private ScheduledExecutorService executorService;

    public static void startScheduler(Order order) {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(
                new FindDeliveryScheduler(order, executorService), 0,
                SchedulersConstants.FIND_DELIVERY_INTERVAL, SchedulersConstants.FIND_DELIVERY_TIME_UNIT);
    }

    public FindDeliveryScheduler(Order order, ScheduledExecutorService executorService) {
        this.order = order;
        this.executorService = executorService;
    }

    private void deliverOrder(Order order, DeliveryInfo deliveryInfo) {
        OrderServiceImpl.getInstance().setOrderDelivery(order, deliveryInfo);
        OrderServiceImpl.getInstance().updateOrderState(order, OrderState.DELIVERY_FOUND);

        DeliverOrderTimer.startTimer(order);
    }

    @Override
    public void run() {
        try {
            DeliveryInfo deliveryInfo = DeliveryServiceImpl.getInstance().findNearestDelivery(
                    this.order.getUser().getLocation(), this.order.getRestaurant().getLocation());
            if (deliveryInfo.getDelivery() == null)
                return;

            deliverOrder(this.order, deliveryInfo);

            this.executorService.shutdownNow();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
