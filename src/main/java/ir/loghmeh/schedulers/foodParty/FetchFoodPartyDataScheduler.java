package ir.loghmeh.schedulers.foodParty;

import ir.loghmeh.services.implementation.APIServiceImpl;
import ir.loghmeh.utilities.constants.SchedulersConstants;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class FetchFoodPartyDataScheduler implements Runnable {
    private static LocalDateTime lastUpdate;

    private static final FetchFoodPartyDataScheduler instance =
            new FetchFoodPartyDataScheduler();

    public static FetchFoodPartyDataScheduler getInstance() {
        return instance;
    }

    private FetchFoodPartyDataScheduler() {
    }

    public static void startScheduler() {
        ScheduledExecutorService executorService =
                Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(
                new FetchFoodPartyDataScheduler(), 0,
                SchedulersConstants.FETCH_FOOD_PARTY_INTERVAL,
                SchedulersConstants.FETCH_FOOD_PARTY_TIME_UNIT);
    }

    private void refreshLastUpdate() {
        lastUpdate = LocalDateTime.now();
    }

    public Integer getRemainingTime() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nextUpdate = lastUpdate.plusMinutes(
                SchedulersConstants.FETCH_FOOD_PARTY_INTERVAL);

        Duration duration = Duration.between(nextUpdate, now);
        return (int)Math.abs(duration.getSeconds());
    }

    @Override
    public void run() {
        try {
            APIServiceImpl.getInstance().fetchFoodPartyData();
            refreshLastUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
