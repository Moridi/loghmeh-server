package ir.loghmeh.models.food.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FoodDTO extends FoodSummaryDTO {
    private String restaurantName;

    public FoodDTO() {
    }

    public FoodDTO(String name, String description,
                   Double popularity, String restaurantName,
                   Integer price, String image,
                   String id) {
        super(name, description, popularity, price, image, id);
        this.restaurantName = restaurantName;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {}
        return null;
    }
}
