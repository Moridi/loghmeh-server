package ir.loghmeh.models.food;

import ir.loghmeh.models.restaurant.Restaurant;

public class Food {
    private Integer id;
    private String name;
    private String description;
    private Double popularity;
    private Restaurant restaurant;
    private Integer price;
    private String image;

    public Food(Integer id, String name,
                String description, Double popularity,
                Restaurant restaurant,
                Integer price, String image) {
        this.name = name;
        this.description = description;
        this.popularity = popularity;
        this.restaurant = restaurant;
        this.price = price;
        this.image = image;
        this.id = id;
    }

    public Food(String name, String description, Double popularity,
                Restaurant restaurant, Integer price, String image) {
        this.name = name;
        this.description = description;
        this.popularity = popularity;
        this.restaurant = restaurant;
        this.price = price;
        this.image = image;
        this.id = null;
    }

    public int getIntegerId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getId() {
        return id.toString();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
