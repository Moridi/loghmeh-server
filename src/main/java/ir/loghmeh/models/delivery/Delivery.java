package ir.loghmeh.models.delivery;

import com.jsoniter.annotation.JsonProperty;
import ir.loghmeh.models.location.Location;

public class Delivery {

    @JsonProperty(required = true)
    private String id;
    @JsonProperty(required = true)
    private Integer velocity;
    @JsonProperty(required = true)
    private Location location;

    public Delivery() {}

    public Delivery(String id, Integer velocity, Location location) {
        this.id = id;
        this.velocity = velocity;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVelocity() {
        return velocity;
    }

    public void setVelocity(Integer velocity) {
        this.velocity = velocity;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}