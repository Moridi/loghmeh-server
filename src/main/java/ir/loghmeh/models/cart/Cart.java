package ir.loghmeh.models.cart;

import ir.loghmeh.models.food.Food;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private List<Food> foods;

    public Cart() {
        this.foods = new ArrayList<>();
    }

    public Cart(List<Food> foods) {
        this.foods = foods;
    }

    public List<Food> getFoods() {
        return foods;
    }

    public Boolean isEmpty() {
        return foods.size() == 0;
    }

    public void addFood(Food food) {
        this.foods.add(food);
    }

    public void removeFood(Food food) {
        this.foods.remove(food);
    }
}
