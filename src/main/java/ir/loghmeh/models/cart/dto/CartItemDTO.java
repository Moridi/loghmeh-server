package ir.loghmeh.models.cart.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsoniter.annotation.JsonProperty;

public class CartItemDTO {
    @JsonProperty(required = true)
    private String foodId;

    @JsonProperty(required = true)
    private String foodName;
    @JsonProperty(required = true)
    private Integer count;
    @JsonProperty(required = true)
    private Integer price;
    @JsonProperty(required = true)
    private String restaurantId;
    public CartItemDTO(Integer count, String foodId, String foodName,
                       Integer price, String restaurantId) {
        this.foodId = foodId;
        this.foodName = foodName;
        this.count = count;
        this.price = price;
        this.restaurantId = restaurantId;
    }

    public CartItemDTO() {
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getFoodId() {
        return foodId;
    }

    public void setFoodId(String foodId) {
        this.foodId = foodId;
    }

    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {}
        return null;
    }
}
