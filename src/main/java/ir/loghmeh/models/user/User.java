package ir.loghmeh.models.user;

import ir.loghmeh.models.location.Location;

public class User {
    private String username;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private Integer balance;
    private Location location;
    private String password;

    public User(String firstName,
                String lastName,
                String phone,
                String username,
                String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.username = username;
        this.email = username;
        this.password = password;
        this.location = new Location(0, 0);
        this.balance = 0;
    }

    public User(String username, String firstName,
                String lastName, String phone,
                String email, Location location) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.location = location;
        this.balance = 0;
    }

    public User(String username, String firstName,
                String lastName, String phone,
                String email, Integer balance,
                Location location) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.balance = balance;
        this.location = location;
    }

    public User(String username, String firstName,
                String lastName, String phone,
                String email, Integer balance,
                Location location, String password) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.balance = balance;
        this.location = location;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
