package ir.loghmeh.models.order;

import ir.loghmeh.models.delivery.Delivery;

public class DeliveryInfo {
    private Delivery delivery;
    private Double deliveryDuration;

    public DeliveryInfo(Delivery delivery, Double deliveryDuration) {
        this.delivery = delivery;
        this.deliveryDuration = deliveryDuration;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Double getDeliveryDuration() {
        return deliveryDuration;
    }

    public void setDeliveryDuration(Double deliveryDuration) {
        this.deliveryDuration = deliveryDuration;
    }
}
