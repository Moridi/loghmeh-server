package ir.loghmeh.models.order;

import ir.loghmeh.models.food.Food;

public class OrderItem {
    Integer count;
    Food food;

    public OrderItem(Food food) {
        this.food = food;
        this.count = 1;
    }

    public OrderItem(Integer count, Food food) {
        this.count = count;
        this.food = food;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public void increaseCount() {
        this.count++;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
