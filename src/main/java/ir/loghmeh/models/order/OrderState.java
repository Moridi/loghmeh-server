package ir.loghmeh.models.order;

public enum OrderState {
    FINDING_DELIVERY,
    DELIVERY_FOUND,
    DELIVERED
}
