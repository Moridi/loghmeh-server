package ir.loghmeh.models.order.dto;

import com.jsoniter.annotation.JsonProperty;
import ir.loghmeh.models.order.OrderState;

import java.util.List;

public class OrderDTO {
    @JsonProperty(required = true)
    private String id;
    @JsonProperty(required = true)
    private String restaurantName;
    @JsonProperty(required = true)
    private List<OrderItemDTO> foods;
    @JsonProperty(required = true)
    private OrderState status;

    public OrderDTO(String id, String restaurantName,
                    List<OrderItemDTO> foods, OrderState status) {
        this.id = id;
        this.restaurantName = restaurantName;
        this.foods = foods;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public List<OrderItemDTO> getFoods() {
        return foods;
    }

    public void setFoods(List<OrderItemDTO> foods) {
        this.foods = foods;
    }

    public OrderState getStatus() {
        return status;
    }

    public void setStatus(OrderState status) {
        this.status = status;
    }
}
