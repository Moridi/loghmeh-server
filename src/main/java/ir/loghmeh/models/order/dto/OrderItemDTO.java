package ir.loghmeh.models.order.dto;

import com.jsoniter.annotation.JsonProperty;

public class OrderItemDTO {
    @JsonProperty(required = true)
    private String foodName;
    @JsonProperty(required = true)
    private Integer count;
    @JsonProperty(required = true)
    private Integer price;

    public OrderItemDTO(Integer count, String foodName,
                       Integer price) {
        this.foodName = foodName;
        this.count = count;
        this.price = price;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }
}
