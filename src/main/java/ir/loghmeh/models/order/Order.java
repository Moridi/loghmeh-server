package ir.loghmeh.models.order;

import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.user.User;

import java.util.ArrayList;
import java.util.List;

public class Order {

    private Integer id;
    private User user;
    private Restaurant restaurant;
    private List<OrderItem> orderItems;
    private OrderState state;
    private DeliveryInfo deliveryInfo;

    public Order(List<OrderItem> orderItems,
                 Restaurant restaurant, User user) {
        this.orderItems = orderItems;
        this.restaurant = restaurant;
        this.user = user;
        this.state = OrderState.FINDING_DELIVERY;
    }

    public Order(Integer id, User user,
                 Restaurant restaurant,
                 OrderState state) {
        this.orderItems = new ArrayList<>();
        this.id = id;
        this.user = user;
        this.restaurant = restaurant;
        this.state = state;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderState getState() {
        return state;
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public DeliveryInfo getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(DeliveryInfo deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

    public String getId() {
        return this.id.toString();
    }

    public Integer getIntegerId() {
        return this.id;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public void addOrderItem(OrderItem orderItem) {
        this.orderItems.add(orderItem);
    }
}
