package ir.loghmeh.models.restaurant;

import ir.loghmeh.models.location.Location;

public class Restaurant {
    private String id;
    private String name;
    private Location location;
    private String logo;

    public Restaurant(String id, String name, Location location, String logo) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.logo = logo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}