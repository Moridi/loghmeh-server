package ir.loghmeh.models.restaurant.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.loghmeh.models.food.dto.FoodSummaryDTO;
import ir.loghmeh.models.location.Location;

import java.util.List;

public class RestaurantDTO extends RestaurantWithMenuDTO {
    private Location location;

    public RestaurantDTO() {
    }

    public RestaurantDTO(String id, String name,
                         Location location,
                         List<FoodSummaryDTO> menu,
                         String logo) {
        super(id, name, menu, logo);
        this.location = location;
    }

    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {}
        return null;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
