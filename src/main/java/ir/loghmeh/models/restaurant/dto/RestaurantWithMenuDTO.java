package ir.loghmeh.models.restaurant.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.loghmeh.models.food.dto.FoodSummaryDTO;
import ir.loghmeh.models.restaurant.dto.RestaurantSummaryDTO;

import java.util.List;

public class RestaurantWithMenuDTO extends RestaurantSummaryDTO {
    private List<FoodSummaryDTO> menu;

    public RestaurantWithMenuDTO() {
    }

    public RestaurantWithMenuDTO(String id, String name,
                                 List<FoodSummaryDTO> menu,
                                 String logo) {
        super(id, name, logo);
        this.menu = menu;
    }

    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {}
        return null;
    }

    public List<FoodSummaryDTO> getMenu() {
        return menu;
    }

    public void setMenu(List<FoodSummaryDTO> menu) {
        this.menu = menu;
    }
}
