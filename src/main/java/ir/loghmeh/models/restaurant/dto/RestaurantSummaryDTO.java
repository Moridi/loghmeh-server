package ir.loghmeh.models.restaurant.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsoniter.annotation.JsonProperty;

public class RestaurantSummaryDTO {
    private String id;
    @JsonProperty(required = true)
    private String name;
    private String logo;

    public RestaurantSummaryDTO() {
    }

    public RestaurantSummaryDTO(String id, String name,
                                String logo) {
        this.id = id;
        this.name = name;
        this.logo = logo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {}
        return null;
    }
}
