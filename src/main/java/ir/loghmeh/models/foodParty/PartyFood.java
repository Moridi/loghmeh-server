package ir.loghmeh.models.foodParty;

import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.restaurant.Restaurant;

public class PartyFood extends Food {
    private Integer oldPrice;
    private Integer count;

    public PartyFood(String name, String description,
                     Double popularity, Restaurant restaurant,
                     Integer price, Integer oldPrice, Integer count, String image) {
        super(name, description, popularity, restaurant, price, image);
        this.oldPrice = oldPrice;
        this.count = count;
    }

    public PartyFood(Integer id, String name, String description,
                     Double popularity, Restaurant restaurant,
                     Integer price, Integer oldPrice, Integer count, String image) {
        super(id, name, description, popularity, restaurant, price, image);
        this.oldPrice = oldPrice;
        this.count = count;
    }

    public Integer getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Integer oldPrice) {
        this.oldPrice = oldPrice;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
