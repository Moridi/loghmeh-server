package ir.loghmeh.models.foodParty.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsoniter.annotation.JsonProperty;
import ir.loghmeh.models.food.dto.FoodDTO;

public class PartyFoodDTO extends FoodDTO {
    @JsonProperty(required = true)
    private Integer oldPrice;
    @JsonProperty(required = true)
    private Integer count;

    @JsonProperty(required = true)
    private String restaurantId;

    public PartyFoodDTO() {
        super();
    }

    public PartyFoodDTO(String name, String description,
                        Double popularity, String restaurantName,
                        Integer price, String image,
                        Integer oldPrice, Integer count,
                        String restaurantId, String id) {
        super(name, description, popularity,
                restaurantName, price, image, id);
        this.oldPrice = oldPrice;
        this.count = count;
        this.restaurantId = restaurantId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {}
        return null;
    }

    public Integer getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Integer oldPrice) {
        this.oldPrice = oldPrice;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
