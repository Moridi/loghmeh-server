package ir.loghmeh.models.foodParty.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsoniter.annotation.JsonProperty;
import ir.loghmeh.models.location.Location;

import java.util.List;

public class FoodPartyRestaurantDTO {
    private String id;
    @JsonProperty(required = true)
    private String name;
    private String description;
    private Location location;
    private List<PartyFoodDTO> menu;
    private String logo;

    public FoodPartyRestaurantDTO() {}

    public FoodPartyRestaurantDTO(String id, String name,
                                  String description, Location location,
                                  List<PartyFoodDTO> menu, String logo) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.location = location;
        this.menu = menu;
        this.logo = logo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<PartyFoodDTO> getMenu() {
        return menu;
    }

    public void setMenu(List<PartyFoodDTO> menu) {
        this.menu = menu;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {}
        return null;
    }
}
