package ir.loghmeh.models.foodParty.dto;

import com.jsoniter.annotation.JsonProperty;

import java.util.List;

public class FoodPartyDTO {
    @JsonProperty(required = true)
    private Integer remainingTime;
    @JsonProperty(required = true)
    private List<PartyFoodDTO> foods;

    public FoodPartyDTO() {
    }

    public FoodPartyDTO(Integer remainingTime, List<PartyFoodDTO> foods) {
        this.remainingTime = remainingTime;
        this.foods = foods;
    }

    public Integer getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(Integer remainingTime) {
        this.remainingTime = remainingTime;
    }

    public List<PartyFoodDTO> getFoods() {
        return foods;
    }

    public void setFoods(List<PartyFoodDTO> foods) {
        this.foods = foods;
    }
}
