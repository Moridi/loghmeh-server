package ir.loghmeh.models.location;

public class Location {
    private Integer x;
    private Integer y;

    public Location() {}

    public Location(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public double calculateDistance(Location location) {
        double xExp = Math.pow(this.x - location.x, 2);
        double yExp = Math.pow(this.y - location.y, 2);
        return Math.sqrt(xExp + yExp);
    }
}
