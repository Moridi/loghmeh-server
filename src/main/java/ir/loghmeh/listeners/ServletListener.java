package ir.loghmeh.listeners;

import ir.loghmeh.schedulers.foodParty.FetchFoodPartyDataScheduler;
import ir.loghmeh.services.implementation.APIServiceImpl;
import ir.loghmeh.services.implementation.OrderServiceImpl;
import ir.loghmeh.services.implementation.UserServiceImpl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.IOException;

@WebListener
public class ServletListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("##################" +
                " Start Initialization ##################");
        try {
            APIServiceImpl.getInstance().fetchInitialData();
            FetchFoodPartyDataScheduler.startScheduler();
            OrderServiceImpl.getInstance().initializeSchedulers();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("##################" +
                " Context Initialized ##################");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Context destroyed");
    }
}