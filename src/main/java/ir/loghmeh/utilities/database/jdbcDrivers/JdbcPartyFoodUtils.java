package ir.loghmeh.utilities.database.jdbcDrivers;

import ir.loghmeh.models.foodParty.PartyFood;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.dataAccess.RestaurantDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcPartyFoodUtils {

    public static PartyFood getPartyFoodFromResults(ResultSet resultSet,
                                             RestaurantDAO restaurantDAO)
            throws SQLException {

        Restaurant restaurant = restaurantDAO.getById(
                resultSet.getString("restaurantId"));
        if (restaurant == null)
            return null;
        return new PartyFood(
                resultSet.getInt("foodId"),
                resultSet.getString("name"),
                resultSet.getString("description"),
                resultSet.getDouble("popularity"),
                restaurant,
                resultSet.getInt("price"),
                resultSet.getInt("oldPrice"),
                resultSet.getInt("count"),
                resultSet.getString("image"));
    }


}
