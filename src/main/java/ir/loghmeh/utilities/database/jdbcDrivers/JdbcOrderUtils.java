package ir.loghmeh.utilities.database.jdbcDrivers;

import ir.loghmeh.models.delivery.Delivery;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.order.DeliveryInfo;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.OrderItem;
import ir.loghmeh.models.order.OrderState;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.user.User;
import ir.loghmeh.utilities.database.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

public class JdbcOrderUtils {

    public static OrderState getOrderState(String state) {
        if(state.equals(OrderState.FINDING_DELIVERY.toString()))
            return OrderState.FINDING_DELIVERY;
        else if(state.equals(OrderState.DELIVERY_FOUND.toString()))
            return OrderState.DELIVERY_FOUND;
        else if(state.equals(OrderState.DELIVERED.toString()))
            return OrderState.DELIVERED;
        return null;
    }

    public static String getOrderStateString(OrderState orderState) {
        return orderState.toString();
    }


    public static OrderItem getOrderItemFromResults(ResultSet resultSet,
                                                    Order order)
            throws SQLException {
        Food food = JdbcFoodUtils.getFoodFromResults(resultSet, order.getRestaurant());
        return new OrderItem(resultSet.getInt("count"), food);
    }

    public static DeliveryInfo getDeliveryFromResult(ResultSet resultSet)
            throws SQLException {
        Delivery delivery = new Delivery(
                resultSet.getString("deliveryId"),
                resultSet.getInt("velocity"),
                new Location(
                        resultSet.getInt("deliveryLocationX"),
                        resultSet.getInt("deliveryLocationY")));

        return new DeliveryInfo(delivery,
                resultSet.getDouble("deliveryDuration"));
    }

    public static Order getOrderFromResults(ResultSet resultSet,
                                            User user)
            throws SQLException {

        Restaurant restaurant = JdbcRestaurantUtils.getRestaurantFromResult(resultSet);

        return new Order(
                resultSet.getInt("userOrderId"),
                user,
                restaurant,
                getOrderState(resultSet.getString("state"))
        );
    }

    public static void setOrderItemPreparedStatement(PreparedStatement statement,
                                                     Order order,
                                                     OrderItem orderItem)
            throws SQLException {
        statement.setInt(1, order.getIntegerId());
        statement.setInt(2, orderItem.getFood().getIntegerId());
        statement.setInt(3, orderItem.getCount());
    }


    public static void setOrderPreparedStatement(PreparedStatement statement,
                                                 Order order)
            throws SQLException {
        statement.setString(1, order.getUser().getUsername());
        statement.setString(2, order.getRestaurant().getId());
    }

    public static void setDeliveryInfoPreparedStatement(PreparedStatement statement,
                                                        DeliveryInfo deliveryInfo,
                                                        String orderId)
            throws SQLException {

        Delivery delivery = deliveryInfo.getDelivery();

        statement.setString(1, delivery.getId());
        statement.setInt(2, delivery.getVelocity());
        statement.setInt(3, delivery.getLocation().getX());
        statement.setInt(4, delivery.getLocation().getY());
        statement.setDouble(5, deliveryInfo.getDeliveryDuration());
        statement.setString(6, orderId);
    }

    public static PreparedStatement addUserOrderStatement(Connection connection)
            throws SQLException {
        return connection.prepareStatement("INSERT INTO userOrder (username, " +
                    "restaurantId, state)" +
                "VALUES (?, ?, DEFAULT)");
    }

    public static PreparedStatement addOrderItemStatement(Connection connection)
            throws SQLException {
        return connection.prepareStatement(
            "INSERT INTO orderItem (userOrderId, foodId, count)" +
            "VALUES (?, ?, ?)");
    }

    public static PreparedStatement getUserOrdersDataStatement(Connection connection)
            throws SQLException {
        return connection.prepareStatement(
                "SELECT * FROM userOrder UO " +
                        "INNER JOIN user U ON UO.username = U.username " +
                        "INNER JOIN restaurant R ON UO.restaurantId = R.restaurantId " +
                "WHERE U.username = ?");
    }

    public static PreparedStatement setUserOrderItemsStatement(Connection connection)
            throws SQLException {
        return connection.prepareStatement(
                "SELECT * FROM orderItem OI " +
                    "INNER JOIN food F ON OI.foodId = F.foodId " +
                "WHERE OI.userOrderId = ?");
    }

    public static PreparedStatement getUserOrdersWithStateDataStatement(Connection connection)
            throws SQLException {

        return connection.prepareStatement(
            "SELECT * FROM userOrder UO " +
                    "INNER JOIN user U ON UO.username = U.username " +
                    "INNER JOIN restaurant R ON UO.restaurantId = R.restaurantId " +
            "WHERE UO.state = ?");
    }

    public static PreparedStatement updateOrderStateStatement(Connection connection)
            throws SQLException {

        return connection.prepareStatement(
                "UPDATE userOrder " +
                        "SET state = ? " +
                "WHERE userOrderId = ?");
    }

    public static PreparedStatement updateOrderDeliveryStatement(Connection connection)
            throws SQLException {
        return connection.prepareStatement(
            "INSERT INTO delivery (deliveryId," +
                    "velocity, deliveryLocationX," +
                    "deliveryLocationY, deliveryDuration," +
                    "userOrderId)" +
            "VALUES (?, ?, ?, ?, ?, ?)");
    }

    public static PreparedStatement getDeliveryInfoStatement(Connection connection)
            throws SQLException {
        return connection.prepareStatement(
                "SELECT * FROM delivery D " +
                      "INNER JOIN userOrder UO ON UO.userOrderId = D.userOrderId " +
                "WHERE UO.userOrderId = ?");
    }

    public static void executeAddUserOrder(PreparedStatement orderStatement,
                                           Order order)
            throws SQLException {

        JdbcOrderUtils.setOrderPreparedStatement(orderStatement, order);
        orderStatement.executeUpdate();
    }

    public static void executeGetUserOrdersData(Connection connection,
                                                PreparedStatement statement,
                                                User user,
                                                Set<Order>  orders)
            throws SQLException {

        statement.setString(1, user.getUsername());
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            Order order = JdbcOrderUtils.getOrderFromResults(resultSet, user);
            orders.add(order);
        }

        DatabaseUtils.closeResultSet(connection, statement, resultSet);
    }

    public static void executeSetUserOrderItems(Connection connection,
                                                PreparedStatement statement,
                                                Order order)
            throws SQLException {

        statement.setInt(1, order.getIntegerId());
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            OrderItem orderItem = JdbcOrderUtils.
                    getOrderItemFromResults(resultSet, order);
            order.addOrderItem(orderItem);
        }

        DatabaseUtils.closeResultSet(connection, statement, resultSet);
    }

    public static void executeGetUserOrdersWithStateData(Connection connection,
                                                         PreparedStatement statement,
                                                         Set<Order> orders,
                                                         OrderState orderState)
            throws SQLException {

        statement.setString(1, JdbcOrderUtils.getOrderStateString(orderState));
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            User user = JdbcUserUtils.getUserFromResult(resultSet);
            Order order = JdbcOrderUtils.getOrderFromResults(resultSet, user);
            orders.add(order);
        }
        DatabaseUtils.closeResultSet(connection, statement, resultSet);
    }

    public static void executeUpdateOrderState(Connection connection,
                                               PreparedStatement statement,
                                               Order order,
                                               OrderState orderState)
            throws SQLException {

        statement.setString(1, JdbcOrderUtils.getOrderStateString(orderState));
        statement.setInt(2, order.getIntegerId());
        statement.executeUpdate();

        DatabaseUtils.closeStatement(connection, statement);
    }

    public static void executeUpdateOrderDelivery(Connection connection,
                                                  PreparedStatement statement,
                                                  DeliveryInfo deliveryInfo,
                                                  Order order)
            throws SQLException {
        JdbcOrderUtils.setDeliveryInfoPreparedStatement(statement,
                deliveryInfo, order.getId());
        statement.executeUpdate();
        DatabaseUtils.closeStatement(connection, statement);
    }

    public static DeliveryInfo executeGetDeliveryInfo(Connection connection,
                                                      PreparedStatement statement,
                                                      Order order)
            throws SQLException {
        DeliveryInfo deliveryInfo = null;

        statement.setInt(1, order.getIntegerId());
        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next())
            deliveryInfo = JdbcOrderUtils.getDeliveryFromResult(resultSet);

        DatabaseUtils.closeResultSet(connection, statement, resultSet);

        return deliveryInfo;
    }
}
