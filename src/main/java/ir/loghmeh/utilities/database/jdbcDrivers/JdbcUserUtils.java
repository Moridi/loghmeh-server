package ir.loghmeh.utilities.database.jdbcDrivers;

import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.user.User;
import ir.loghmeh.utilities.database.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcUserUtils {

    public static User getUserFromResult(ResultSet result)
            throws SQLException {
        return new User(
                result.getString("username"),
                result.getString("firstName"),
                result.getString("lastName"),
                result.getString("phone"),
                result.getString("email"),
                result.getInt("balance"),
                new Location(result.getInt("userLocationX"),
                        result.getInt("userLocationY")),
                result.getString("password")
        );
    }

    public static void setUserPreparedStatement(PreparedStatement preparedStatement,
                                                User user)
            throws SQLException {
        preparedStatement.setString(1, user.getUsername());
        preparedStatement.setString(2, user.getFirstName());
        preparedStatement.setString(3, user.getLastName());
        preparedStatement.setString(4, user.getPhone());
        preparedStatement.setString(5, user.getEmail());
        preparedStatement.setInt(6, user.getBalance());
        preparedStatement.setInt(7, user.getLocation().getX());
        preparedStatement.setInt(8, user.getLocation().getY());
        preparedStatement.setString(9, user.getPassword());
    }

    public static PreparedStatement addStatement(Connection connection)
            throws SQLException {
        return connection.prepareStatement(
                "INSERT INTO user(username," +
                        "firstName, lastName," +
                        "phone, email, balance," +
                        "userLocationX, userLocationY," +
                        "password)" +
                "VALUES(?,?,?,?,?,?,?,?,?)");
    }


    public static PreparedStatement getByNameStatement(Connection connection)
            throws SQLException {
        return connection.prepareStatement(
                "SELECT * FROM user WHERE username = ?");
    }

    public static PreparedStatement updateUserBalanceStatement(Connection connection)
            throws SQLException {
        return connection.prepareStatement(
                "UPDATE user SET balance = ? WHERE username = ?");
    }

    public static void executeAdd(Connection connection,
                                  PreparedStatement preparedStatement,
                                  User user)
            throws SQLException {

        JdbcUserUtils.setUserPreparedStatement(preparedStatement, user);
        preparedStatement.executeUpdate();

        DatabaseUtils.closeStatement(connection, preparedStatement);
    }

    public static User executeGetByName(Connection connection,
                                        PreparedStatement statement,
                                        String username)
            throws SQLException {
        statement.setString(1, username);

        ResultSet result = statement.executeQuery();
        result.next();

        User user = JdbcUserUtils.getUserFromResult(result);

        DatabaseUtils.closeResultSet(connection, statement, result);

        return user;
    }

    public static void executeUpdateUserBalance(Connection connection,
                                                PreparedStatement preparedStatement,
                                                User user,
                                                Integer amount)
            throws SQLException {

        preparedStatement.setInt(1, user.getBalance() + amount);
        preparedStatement.setString(2, user.getUsername());

        preparedStatement.executeUpdate();

        DatabaseUtils.closeStatement(connection, preparedStatement);
    }
}
