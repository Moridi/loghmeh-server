package ir.loghmeh.utilities.database.jdbcDrivers;

import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.restaurant.Restaurant;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcFoodUtils {

    public static Food getFoodFromResults(ResultSet resultSet,
                                          Restaurant restaurant)
            throws SQLException {
        return new Food(
                resultSet.getInt("foodId"),
                resultSet.getString("name"),
                resultSet.getString("description"),
                resultSet.getDouble("popularity"),
                restaurant,
                resultSet.getInt("price"),
                resultSet.getString("image"));
    }

    public static void setFoodPreparedStatement(PreparedStatement statement,
                                                Food food)
            throws SQLException {
        statement.setString(1, food.getName());
        statement.setString(2, food.getDescription());
        statement.setDouble(3, food.getPopularity());
        statement.setInt(4, food.getPrice());
        statement.setString(5, food.getImage());
    }

    public static void setMenuItemPreparedStatement(PreparedStatement statement,
                                                    Food food)
            throws SQLException {
        statement.setString(1, food.getRestaurant().getId());
        statement.setInt(2, food.getIntegerId());
    }
}
