package ir.loghmeh.utilities.database.jdbcDrivers;

import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.restaurant.Restaurant;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcRestaurantUtils {
    public static Restaurant getRestaurantFromResult(ResultSet resultSet)
            throws SQLException {
        return new Restaurant(
                resultSet.getString("restaurantId"),
                resultSet.getString("name"),
                new Location(
                        resultSet.getInt("restaurantLocationX"),
                        resultSet.getInt("restaurantLocationY")),
                resultSet.getString("logo"));
    }
}
