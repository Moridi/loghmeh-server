package ir.loghmeh.utilities.database;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseUtils {

    private final static String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    private final static String DRIVER_PREFIX = "jdbc:";
    private final static String MYSQL_URL = "mysql://mysql/loghmeh";
    private final static String MYSQL_FLAGS = "?useUnicode=yes&characterEncoding=UTF-8&" +
            "allowPublicKeyRetrieval=true&useSSL=false";
            
    private final static String USERNAME = "root";
    private final static String PASSWORD = "rootroot";

    public static ComboPooledDataSource initializeDataSource() {
        try {
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        ComboPooledDataSource dataSource = new ComboPooledDataSource();

        dataSource.setJdbcUrl(DRIVER_PREFIX + MYSQL_URL + MYSQL_FLAGS);
        dataSource.setUser(USERNAME);
        dataSource.setPassword(PASSWORD);

        dataSource.setInitialPoolSize(5);
        dataSource.setMinPoolSize(5);
        dataSource.setAcquireIncrement(5);
        dataSource.setMaxPoolSize(20);
        dataSource.setMaxStatements(100);

        return dataSource;
    }

    // @TODO: Add from table
    public static Integer getLastId(Connection connection) {
        Integer lastId = null;

        try {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT LAST_INSERT_ID() as last_id");

            ResultSet result = statement.executeQuery();

            if (result.next())
                lastId = result.getInt("last_id");

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lastId;
    }

    public static void closeStatement(Connection connection,
                                      PreparedStatement statement)
            throws SQLException {
        statement.close();
        connection.close();
    }

    public static void closeResultSet(Connection connection,
                                      PreparedStatement statement,
                                      ResultSet resultSet)
            throws SQLException {
        resultSet.close();
        closeStatement(connection, statement);
    }
}
