package ir.loghmeh.utilities.constants;

public class ModelsConstants {
    public static final Integer DISTANCE_THRESHOLD = 170;
    public static final Integer RESTAURANT_BUCKET_SIZE = 15;
}
