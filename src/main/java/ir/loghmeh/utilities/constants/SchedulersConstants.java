package ir.loghmeh.utilities.constants;

import java.util.concurrent.TimeUnit;

public class SchedulersConstants {
    public static final Integer FETCH_FOOD_PARTY_INTERVAL = 30;
    public static final TimeUnit FETCH_FOOD_PARTY_TIME_UNIT = TimeUnit.MINUTES;

    public static final Integer FIND_DELIVERY_INTERVAL = 30;
    public static final TimeUnit FIND_DELIVERY_TIME_UNIT = TimeUnit.SECONDS;
}
