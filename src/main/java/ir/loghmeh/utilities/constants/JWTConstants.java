package ir.loghmeh.utilities.constants;

public class JWTConstants {
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "loghmeh.ir";
    public static String SECRET_KEY = "loghmehloghmehloghmehloghmehloghmeh";
    public static String WEB_CLIENT_ID = "212344169081-15vg4fvkpofaln4e7382dlma582dod5t.apps.googleusercontent.com";
    public static final Long LIFE_TIME = 86400000L;
    public static final Integer SALT_SEED = 12;
}
