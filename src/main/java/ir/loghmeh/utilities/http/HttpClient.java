package ir.loghmeh.utilities.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpClient {

    private static final HttpClient instance = new HttpClient();

    public static HttpClient getInstance() {
        return instance;
    }

    public String sendGet(String url) throws IOException {
        HttpURLConnection httpClient = (HttpURLConnection)
                new URL(url).openConnection();
        httpClient.setRequestMethod("GET");

        StringBuilder response = new StringBuilder();

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(httpClient.getInputStream()))) {
            String line;
            while ((line = in.readLine()) != null) {
                response.append(line);
            }
        }

        return response.toString();
    }
}
