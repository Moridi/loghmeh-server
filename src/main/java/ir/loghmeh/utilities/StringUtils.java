package ir.loghmeh.utilities;

public class StringUtils {

    public static Boolean isNumeric(String str) {
        if (str == null || str.length() == 0)
            return false;

        for (char c : str.toCharArray())
            if (!Character.isDigit(c))
                return false;

        return true;
    }

    public static Boolean isPositive(String str)
            throws NumberFormatException{
        if (isNumeric(str))
            return Integer.parseInt(str) > 0;
        return false;
    }
}
