package ir.loghmeh.dataAccess;

import ir.loghmeh.models.order.DeliveryInfo;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.OrderState;
import ir.loghmeh.models.user.User;

import java.util.Set;

public interface OrderDAO {
    void add(Order order);

    Set<Order> getUserOrders(User user);

    void updateOrderState(Order order,
                          OrderState state);

    void updateOrderDelivery(Order order,
                             DeliveryInfo deliveryInfo);


    Set<Order> getUserOrdersWithState(OrderState orderState);
    DeliveryInfo getDeliveryInfo(Order order);
}
