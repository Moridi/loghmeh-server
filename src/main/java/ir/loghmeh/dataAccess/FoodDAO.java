package ir.loghmeh.dataAccess;

import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.restaurant.Restaurant;

import java.util.Set;

public interface FoodDAO {
    void add(Food food);

    Set<Food> getFoodsByRestaurant(Restaurant restaurant);

    Food getFood(String foodId);
}
