package ir.loghmeh.dataAccess;

import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.user.User;

import java.util.List;
import java.util.Set;

public interface RestaurantDAO {
    Restaurant getByName(String name);

    Restaurant getById(String id);

    void add(Restaurant restaurant);

    void saveAll(Set<Restaurant> restaurants);

    List<Restaurant> getUserNearRestaurants(User user, int startAt);

    List<Restaurant> searchByNameAndFoodName(String name, String foodName,
                                             User user, int startAt);
}
