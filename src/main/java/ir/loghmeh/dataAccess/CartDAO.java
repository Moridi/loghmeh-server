package ir.loghmeh.dataAccess;

import ir.loghmeh.models.cart.Cart;
import ir.loghmeh.models.food.Food;

public interface CartDAO {

    Cart getCart(String username);

    void addToCart(String username, Food food);

    void clearCart(String username);

    void removeFromCart(String username, Food cartFood);
}
