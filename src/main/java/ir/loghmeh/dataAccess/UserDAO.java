package ir.loghmeh.dataAccess;

import ir.loghmeh.models.user.User;

public interface UserDAO {
    void add(User user);

    User getByName(String username);

    void updateUserBalance(User user,
                           Integer amount);
}
