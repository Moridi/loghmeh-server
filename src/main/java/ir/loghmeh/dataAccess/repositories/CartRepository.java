package ir.loghmeh.dataAccess.repositories;

import ir.loghmeh.models.cart.Cart;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.dataAccess.CartDAO;

import java.util.HashMap;
import java.util.Map;

public class CartRepository implements CartDAO {
    private static final CartRepository instance =
            new CartRepository();
    private Map<String, Cart> cartMap;

    private CartRepository() {
        cartMap = new HashMap<>();
    }

    public static CartRepository getInstance() {
        return instance;
    }

    @Override
    public Cart getCart(String username) {
        if (!cartMap.containsKey(username))
            cartMap.put(username, new Cart());
        return cartMap.get(username);
    }

    @Override
    public void addToCart(String username, Food food) {
        cartMap.get(username).addFood(food);
    }

    @Override
    public void removeFromCart(String username, Food cartFood) {
        cartMap.get(username).removeFood(cartFood);
    }

    @Override
    public void clearCart(String username) {
        cartMap.get(username).getFoods().clear();
    }
}
