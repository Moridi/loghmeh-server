package ir.loghmeh.dataAccess;

import ir.loghmeh.models.foodParty.PartyFood;

import java.util.Set;

public interface PartyFoodDAO {
    void add(PartyFood partyFood);

    Set<PartyFood> getAll();

    PartyFood getByFoodId(String foodId);

    void expire();

    void decreaseFoodCount(PartyFood partyFood, int amount);
}
