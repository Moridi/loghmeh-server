package ir.loghmeh.dataAccess.jdbcDrivers;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import ir.loghmeh.models.foodParty.PartyFood;
import ir.loghmeh.dataAccess.FoodDAO;
import ir.loghmeh.dataAccess.PartyFoodDAO;
import ir.loghmeh.dataAccess.RestaurantDAO;
import ir.loghmeh.utilities.database.DatabaseUtils;
import ir.loghmeh.utilities.database.jdbcDrivers.JdbcPartyFoodUtils;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class JdbcPartyFoodDAO implements PartyFoodDAO {

    private static final JdbcPartyFoodDAO instance = new JdbcPartyFoodDAO();

    private ComboPooledDataSource dataSource;

    private final FoodDAO foodDAO = JdbcFoodDAO.getInstance();

    private final RestaurantDAO restaurantDAO = JdbcRestaurantDAO.getInstance();

    public static JdbcPartyFoodDAO getInstance() {
        return instance;
    }

    public JdbcPartyFoodDAO() {
        dataSource = DatabaseUtils.initializeDataSource();
    }

    @Override
    public void add(PartyFood partyFood) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO partyFood (foodId, oldPrice, count, expired)" +
                            "VALUES (?, ?, ?, DEFAULT)");

            foodDAO.add(partyFood);

            statement.setInt(1, partyFood.getIntegerId());
            statement.setInt(2, partyFood.getOldPrice());
            statement.setInt(3, partyFood.getCount());
            statement.executeUpdate();

            DatabaseUtils.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Set<PartyFood> getAll() {
        Set<PartyFood> foods = new HashSet<>();
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM partyFood PF " +
                            "INNER JOIN food F ON PF.foodId = F.foodId " +
                            "INNER JOIN menuItem M ON PF.foodId = M.foodId " +
                        "WHERE PF.expired = false");

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                PartyFood partyFood = JdbcPartyFoodUtils.
                    getPartyFoodFromResults(resultSet, restaurantDAO);
                if (partyFood != null)
                    foods.add(partyFood);

            }

            DatabaseUtils.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return foods;
    }

    @Override
    public PartyFood getByFoodId(String foodId) {
        PartyFood food = null;
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM partyFood PF " +
                            "INNER JOIN food F ON PF.foodId = F.foodId " +
                            "INNER JOIN menuItem M ON PF.foodId = M.foodId " +
                            "WHERE F.foodId = ?");
            statement.setInt(1, Integer.parseInt(foodId));


            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
                food = JdbcPartyFoodUtils.getPartyFoodFromResults(
                        resultSet, restaurantDAO);

            DatabaseUtils.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return food;
    }

    @Override
    public void expire() {
        try {
            Connection connection = dataSource.getConnection();

            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE partyFood SET expired = true WHERE expired = false");
            statement.executeUpdate();

            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decreaseFoodCount(PartyFood partyFood, int amount) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE partyFood SET count = count - ? " +
                            "WHERE foodId = ?"
            );

            statement.setInt(1, amount);
            statement.setInt(2, partyFood.getIntegerId());
            statement.executeUpdate();

            DatabaseUtils.closeStatement(connection, statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}