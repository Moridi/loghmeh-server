package ir.loghmeh.dataAccess.jdbcDrivers;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.dataAccess.FoodDAO;
import ir.loghmeh.dataAccess.RestaurantDAO;
import ir.loghmeh.utilities.database.DatabaseUtils;
import ir.loghmeh.utilities.database.jdbcDrivers.JdbcFoodUtils;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class JdbcFoodDAO implements FoodDAO {

    private static final JdbcFoodDAO instance = new JdbcFoodDAO();

    private ComboPooledDataSource dataSource;

    private final RestaurantDAO restaurantDAO =
            JdbcRestaurantDAO.getInstance();

    public static JdbcFoodDAO getInstance() {
        return instance;
    }

    public JdbcFoodDAO() {
        dataSource = DatabaseUtils.initializeDataSource();
    }

    private void addToFoodTable(Food food) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement foodStatement = connection.prepareStatement(
                    "INSERT INTO food (name, description," +
                            "popularity, price, image)" +
                            "VALUES (?, ?, ?, ?, ?)");

            JdbcFoodUtils.setFoodPreparedStatement(foodStatement, food);
            foodStatement.executeUpdate();
            Integer lastId = DatabaseUtils.getLastId(connection);
            food.setId(lastId);

            DatabaseUtils.closeStatement(connection, foodStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addToMenuItem(Food food) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement menuItemStatement = connection.prepareStatement(
                    "INSERT INTO menuItem (restaurantId, foodId)" +
                            "VALUES (?, ?)");

            JdbcFoodUtils.setMenuItemPreparedStatement(
                    menuItemStatement, food);

            menuItemStatement.executeUpdate();

            DatabaseUtils.closeStatement(connection, menuItemStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void add(Food food) {
        addToFoodTable(food);
        addToMenuItem(food);
    }

    @Override
    public Set<Food> getFoodsByRestaurant(Restaurant restaurant) {
        Set<Food> foods = new HashSet<>();

        if (restaurant == null) return null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM food F " +
                            "INNER JOIN menuItem M ON F.foodId = M.foodId " +
                            "WHERE restaurantId = ?");
            statement.setString(1, restaurant.getId());

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Food food = JdbcFoodUtils.getFoodFromResults(
                        resultSet, restaurant);
                foods.add(food);
            }

            DatabaseUtils.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return foods;
    }

    @Override
    public Food getFood(String foodId) {
        Food food = null;
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM food F " +
                            "INNER JOIN menuItem M ON F.foodId = M.foodId " +
                            "WHERE F.foodId = ?");
            statement.setInt(1, Integer.parseInt(foodId));

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Restaurant restaurant = restaurantDAO.getById(
                        resultSet.getString("restaurantId"));
                if (restaurant == null)
                    return null;
                food = JdbcFoodUtils.getFoodFromResults(
                        resultSet, restaurant);
            }

            DatabaseUtils.closeResultSet(connection, statement, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return food;
    }
}