package ir.loghmeh.dataAccess.jdbcDrivers;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import ir.loghmeh.models.order.DeliveryInfo;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.OrderItem;
import ir.loghmeh.models.order.OrderState;
import ir.loghmeh.models.user.User;
import ir.loghmeh.dataAccess.OrderDAO;
import ir.loghmeh.utilities.database.DatabaseUtils;
import ir.loghmeh.utilities.database.jdbcDrivers.JdbcOrderUtils;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class JdbcOrderDAO implements OrderDAO {
    private final static JdbcOrderDAO instance =
            new JdbcOrderDAO();

    private ComboPooledDataSource dataSource;

    public static JdbcOrderDAO getInstance() {
        return instance;
    }

    private JdbcOrderDAO() {
        dataSource = DatabaseUtils.initializeDataSource();
    }

    private void addUserOrder(Order order) {
        try {
            Connection connection = dataSource.getConnection();

            PreparedStatement orderStatement =
                    JdbcOrderUtils.addUserOrderStatement(connection);

            JdbcOrderUtils.executeAddUserOrder(orderStatement, order);

            Integer lastId = DatabaseUtils.getLastId(connection);
            order.setId(lastId);

            DatabaseUtils.closeStatement(connection, orderStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addOrderItem(Connection connection,
                              Order order,
                              OrderItem orderItem)
            throws SQLException {

        PreparedStatement orderItemStatement =
                JdbcOrderUtils.addOrderItemStatement(connection);

        JdbcOrderUtils.setOrderItemPreparedStatement(
                orderItemStatement, order, orderItem);

        orderItemStatement.executeUpdate();
        orderItemStatement.close();
    }

    @Override
    public void add(Order order) {
        addUserOrder(order);

        try {
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            for (OrderItem orderItem : order.getOrderItems())
                addOrderItem(connection, order, orderItem);

            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Set<Order> getUserOrdersData(User user) {
        Set<Order> orders = new HashSet<>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = JdbcOrderUtils.
                    getUserOrdersDataStatement(connection);

            JdbcOrderUtils.executeGetUserOrdersData(
                    connection, statement, user, orders);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    private void setUserOrderItems(Order order) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = JdbcOrderUtils.
                    setUserOrderItemsStatement(connection);

            JdbcOrderUtils.executeSetUserOrderItems(
                    connection, statement, order);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Set<Order> getUserOrdersWithStateData(OrderState orderState) {
        Set<Order> orders = new HashSet<>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = JdbcOrderUtils.
                    getUserOrdersWithStateDataStatement(connection);

            JdbcOrderUtils.executeGetUserOrdersWithStateData(
                    connection, statement, orders, orderState);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public Set<Order> getUserOrdersWithState(OrderState orderState) {
        Set<Order> orders = getUserOrdersWithStateData(orderState);

        for (Order order : orders)
            setUserOrderItems(order);

        return orders;
    }

    @Override
    public Set<Order> getUserOrders(User user) {
        Set<Order> orders = getUserOrdersData(user);

        for (Order order : orders)
            setUserOrderItems(order);

        return orders;
    }

    @Override
    public void updateOrderState(Order order, OrderState state) {
        try {
            Connection connection = dataSource.getConnection();

            PreparedStatement statement = JdbcOrderUtils.
                    updateOrderStateStatement(connection);

            JdbcOrderUtils.executeUpdateOrderState(
                    connection, statement, order, state);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateOrderDelivery(Order order,
                                    DeliveryInfo deliveryInfo) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement deliveryStatement = JdbcOrderUtils.
                    updateOrderDeliveryStatement(connection);

            JdbcOrderUtils.executeUpdateOrderDelivery(
                    connection, deliveryStatement, deliveryInfo, order);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public DeliveryInfo getDeliveryInfo(Order order) {
        DeliveryInfo deliveryInfo = null;
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = JdbcOrderUtils.
                    getDeliveryInfoStatement(connection);

            deliveryInfo = JdbcOrderUtils.executeGetDeliveryInfo(
                    connection, statement, order);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return deliveryInfo;
    }
}