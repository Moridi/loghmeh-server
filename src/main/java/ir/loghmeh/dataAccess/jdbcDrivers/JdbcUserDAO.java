package ir.loghmeh.dataAccess.jdbcDrivers;


import com.mchange.v2.c3p0.ComboPooledDataSource;
import ir.loghmeh.models.user.User;
import ir.loghmeh.dataAccess.UserDAO;
import ir.loghmeh.utilities.database.DatabaseUtils;
import ir.loghmeh.utilities.database.jdbcDrivers.JdbcUserUtils;

import java.sql.*;

public class JdbcUserDAO implements UserDAO {
    private final static JdbcUserDAO instance =
            new JdbcUserDAO();

    private ComboPooledDataSource dataSource;

    public static JdbcUserDAO getInstance() {
        return instance;
    }

    private JdbcUserDAO() {
        dataSource = DatabaseUtils.initializeDataSource();
    }

    @Override
    public void add(User user) {
        try {
            Connection connection = dataSource.getConnection();

            PreparedStatement preparedStatement = JdbcUserUtils.
                    addStatement(connection);

            JdbcUserUtils.executeAdd(connection, preparedStatement, user);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getByName(String username) {
        User user = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = JdbcUserUtils.
                    getByNameStatement(connection);

            user = JdbcUserUtils.executeGetByName(
                    connection, statement, username);

        } catch (SQLException e) {
        }

        return user;
    }

    @Override
    public void updateUserBalance(User user, Integer amount) {
        try {
            Connection connection = dataSource.getConnection();

            PreparedStatement preparedStatement =
                    JdbcUserUtils.updateUserBalanceStatement(connection);

            JdbcUserUtils.executeUpdateUserBalance(
                    connection, preparedStatement, user, amount);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
