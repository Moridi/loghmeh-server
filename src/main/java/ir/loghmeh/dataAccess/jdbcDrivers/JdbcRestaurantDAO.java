package ir.loghmeh.dataAccess.jdbcDrivers;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.user.User;
import ir.loghmeh.dataAccess.RestaurantDAO;
import ir.loghmeh.utilities.constants.ModelsConstants;
import ir.loghmeh.utilities.constants.SchedulersConstants;
import ir.loghmeh.utilities.database.DatabaseUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class JdbcRestaurantDAO implements RestaurantDAO {
    private static final JdbcRestaurantDAO instance = new JdbcRestaurantDAO();

    private ComboPooledDataSource dataSource;

    public static JdbcRestaurantDAO getInstance() {
        return instance;
    }

    public JdbcRestaurantDAO() {
        dataSource = DatabaseUtils.initializeDataSource();
    }

    private Restaurant getRestaurantFromResult(ResultSet resultSet)
            throws SQLException {
        return new Restaurant(
                resultSet.getString("restaurantId"),
                resultSet.getString("name"),
                new Location(
                        resultSet.getInt("restaurantLocationX"),
                        resultSet.getInt("restaurantLocationY")),
                resultSet.getString("logo"));
    }

    private void setRestaurantPreparedStatement(
            PreparedStatement statement,
            Restaurant restaurant, int base) throws SQLException {
        statement.setString(base + 1, restaurant.getId());
        statement.setString(base + 2, restaurant.getName());
        statement.setString(base + 3, restaurant.getLogo());
        statement.setInt(base + 4, restaurant.getLocation().getX());
        statement.setInt(base + 5, restaurant.getLocation().getY());
    }

    @Override
    public Restaurant getByName(String name) {
        Restaurant restaurant = null;
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM restaurant WHERE name = ?");
            statement.setString(1, name);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
                restaurant = getRestaurantFromResult(resultSet);

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return restaurant;
    }

    @Override
    public Restaurant getById(String id) {
        Restaurant restaurant = null;
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM restaurant WHERE restaurantId = ?");
            statement.setString(1, id);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
                restaurant = getRestaurantFromResult(resultSet);

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return restaurant;
    }

    @Override
    public void add(Restaurant restaurant) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO restaurant (restaurantId, name, logo," +
                            "restaurantLocationX, restaurantLocationY)" +
                            "VALUES (?, ?, ?, ?, ?)"
            );
            setRestaurantPreparedStatement(statement, restaurant, 0);
            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveAll(Set<Restaurant> restaurants) {
        if (restaurants.size() == 0) return;
        try {
            Connection connection = dataSource.getConnection();
            StringBuilder statementLiteral =
                    new StringBuilder(
                            "INSERT INTO restaurant (restaurantId, name, logo," +
                                    "locationX, locationY)" +
                                    "VALUES ");

            for (int i = 0; i < restaurants.size(); i++) {
                statementLiteral.append("(?, ?, ?, ?, ?)");
                if (i < restaurants.size() - 1)
                    statementLiteral.append(", ");
            }

            PreparedStatement statement = connection.prepareStatement(
                    statementLiteral.toString());

            int base = 0;
            for (Restaurant restaurant : restaurants) {
                setRestaurantPreparedStatement(statement, restaurant, base);
                base += 5;
            }

            statement.executeUpdate();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Restaurant> getUserNearRestaurants(User user, int startAt) {
        List<Restaurant> restaurants = new ArrayList<>();
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM restaurant R " +
                        "WHERE SQRT(POW(R.restaurantLocationX - ?, 2) + " +
                                "POW(R.restaurantLocationY - ?, 2)) < ? " +
                        "ORDER BY R.name ASC " +
                        "LIMIT ?, ?");
            statement.setInt(1, user.getLocation().getX());
            statement.setInt(2, user.getLocation().getY());
            statement.setInt(3, ModelsConstants.DISTANCE_THRESHOLD);
            statement.setInt(4, startAt);
            statement.setInt(5, ModelsConstants.RESTAURANT_BUCKET_SIZE);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Restaurant restaurant = getRestaurantFromResult(resultSet);
                restaurants.add(restaurant);
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return restaurants;
    }

    @Override
    public List<Restaurant> searchByNameAndFoodName(String name, String foodName,
                                                    User user, int startAt) {
        List<Restaurant> restaurants = new ArrayList<>();
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM restaurant R " +
                        "WHERE name LIKE ? AND SQRT(POW(R.restaurantLocationX - ?, 2) + " +
                            "POW(R.restaurantLocationY - ?, 2)) < ? AND R.restaurantId IN ( " +
                                "SELECT M.restaurantId FROM food F " +
                                "INNER JOIN menuItem M ON F.foodId = M.foodId " +
                                "WHERE F.name LIKE ?)" +
                        "ORDER BY R.name ASC " +
                        "LIMIT ?, ?");
            statement.setString(1, "%" + name + "%");
            statement.setInt(2, user.getLocation().getX());
            statement.setInt(3, user.getLocation().getY());
            statement.setInt(4, ModelsConstants.DISTANCE_THRESHOLD);
            statement.setString(5, "%" + foodName + "%");
            statement.setInt(6, startAt);
            statement.setInt(7, ModelsConstants.RESTAURANT_BUCKET_SIZE);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Restaurant restaurant = getRestaurantFromResult(resultSet);
                restaurants.add(restaurant);
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return restaurants;
    }
}
