package ir.loghmeh.services;

import ir.loghmeh.exceptions.cart.EmptyCartException;
import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.foodParty.PartyFoodExpired;
import ir.loghmeh.exceptions.foodParty.PartyFoodSoldOut;
import ir.loghmeh.exceptions.order.NoSuchOrderException;
import ir.loghmeh.exceptions.restaurant.DistantRestaurantException;
import ir.loghmeh.exceptions.restaurant.ImproperRestaurantException;
import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.exceptions.user.InvalidUserData;
import ir.loghmeh.exceptions.user.NoCreditException;
import ir.loghmeh.models.cart.Cart;
import ir.loghmeh.models.cart.dto.CartDTO;
import ir.loghmeh.models.cart.dto.CartItemDTO;
import ir.loghmeh.models.foodParty.dto.FoodPartyDTO;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.dto.OrderDTO;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.restaurant.dto.RestaurantSummaryDTO;
import ir.loghmeh.models.restaurant.dto.RestaurantWithMenuDTO;
import ir.loghmeh.models.user.User;
import ir.loghmeh.models.user.dto.UserDTO;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

public interface ApplicationService {

    void chargeUserAccount(String username,
                           Integer amount);
    UserDTO getUserDTO(String username) throws InvalidUserData;

    User getUser(String username);

    Boolean doesUserExist(String username);

    Restaurant getRestaurantByName(String restaurantName)
            throws RestaurantNotFoundException;

    void registerUser(String firstName,
                      String lastName,
                      String phone,
                      String username,
                      String password);

    List<RestaurantSummaryDTO> getNearRestaurantsDTO(String username, int startAt);

    RestaurantWithMenuDTO getNearRestaurantDTOById(String username,
                                                   String id)
            throws DistantRestaurantException,
            RestaurantNotFoundException;

    List<RestaurantSummaryDTO> searchRestaurants(String restaurantName, String foodName,
                                                 String username, int startAt);

    CartDTO getUserCartDTO(String username);

    Cart getUserCart(String username);

    void addToCart(String username,
                   String foodId)
            throws FoodNotFoundException,
            ImproperRestaurantException,
            DistantRestaurantException;

    Order finalizeOrder(String username)
            throws EmptyCartException,
            NoCreditException,
            PartyFoodExpired,
            PartyFoodSoldOut,
            FoodNotFoundException;

    void removeFromCart(String username, String foodId)
            throws FoodNotFoundException;

    Cart getFinalUserCart(User user)
            throws EmptyCartException,
            NoCreditException;

    FoodPartyDTO getFoodPartyDTO();

    List<OrderDTO> getOrdersDTO(String username);

    OrderDTO getOrderDTOById(String username,
                             String id)
            throws NoSuchOrderException;

    void initializeCart(String username, List<CartItemDTO> cartDTO)
            throws ImproperRestaurantException,
            DistantRestaurantException,
            FoodNotFoundException;
}