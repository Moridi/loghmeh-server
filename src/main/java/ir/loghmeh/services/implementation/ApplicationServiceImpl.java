package ir.loghmeh.services.implementation;

import ir.loghmeh.exceptions.cart.EmptyCartException;
import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.foodParty.PartyFoodExpired;
import ir.loghmeh.exceptions.foodParty.PartyFoodSoldOut;
import ir.loghmeh.exceptions.order.NoSuchOrderException;
import ir.loghmeh.exceptions.restaurant.DistantRestaurantException;
import ir.loghmeh.exceptions.restaurant.ImproperRestaurantException;
import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.exceptions.user.InvalidUserData;
import ir.loghmeh.exceptions.user.NoCreditException;
import ir.loghmeh.models.cart.Cart;
import ir.loghmeh.models.cart.dto.CartDTO;
import ir.loghmeh.models.cart.dto.CartItemDTO;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.foodParty.PartyFood;
import ir.loghmeh.models.foodParty.dto.FoodPartyDTO;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.OrderItem;
import ir.loghmeh.models.order.dto.OrderDTO;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.restaurant.dto.RestaurantSummaryDTO;
import ir.loghmeh.models.restaurant.dto.RestaurantWithMenuDTO;
import ir.loghmeh.models.user.User;
import ir.loghmeh.models.user.dto.UserDTO;
import ir.loghmeh.schedulers.delivery.FindDeliveryScheduler;
import ir.loghmeh.services.ApplicationService;
import ir.loghmeh.services.CartService;
import ir.loghmeh.services.FoodService;
import ir.loghmeh.services.FoodPartyService;
import ir.loghmeh.services.OrderService;
import ir.loghmeh.services.RestaurantService;
import ir.loghmeh.services.UserService;
import ir.loghmeh.utilities.constants.ModelsConstants;
import ir.loghmeh.utilities.constants.SchedulersConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ApplicationServiceImpl implements ApplicationService {

    private static final ApplicationServiceImpl instance =
            new ApplicationServiceImpl();
    private FoodService foodService;
    private RestaurantService restaurantService;
    private CartService cartService;
    private UserService userService;
    private OrderService orderService;
    private FoodPartyService foodPartyService;

    public ApplicationServiceImpl() {
        foodService = FoodServiceImpl.getInstance();
        restaurantService = RestaurantServiceImpl.getInstance();
        cartService = CartServiceImpl.getInstance();
        userService = UserServiceImpl.getInstance();
        orderService = OrderServiceImpl.getInstance();
        foodPartyService = FoodPartyServiceImpl.getInstance();
    }

    public static ApplicationServiceImpl getInstance() {
        return instance;
    }

    @Override
    public User getUser(String username) {
        return userService.getUser(username);
    }

    @Override
    public UserDTO getUserDTO(String username)
            throws InvalidUserData {
        User user = userService.getUser(username);

        if (user == null)
            throw new InvalidUserData();

        return userService.getUserDTO(user);
    }

    @Override
    public Boolean doesUserExist(String username) {
        return userService.isUserExists(username);
    }

    @Override
    public void registerUser(String firstName, String lastName, String phone,
                      String username, String password) {
        userService.registerUser(firstName, lastName, phone, username, password);
    }

    @Override
    public void chargeUserAccount(String username,
                                  Integer amount) {
        userService.updateUserBalance(userService.getUser(username), amount);
    }

    @Override
    public Restaurant getRestaurantByName(String restaurantName)
            throws RestaurantNotFoundException {
        return restaurantService.findRestaurantByName(restaurantName);
    }

    private void checkRestaurantDistance(User user,
                                         Restaurant restaurant)
            throws DistantRestaurantException {

        double distance = user.getLocation().calculateDistance(
                restaurant.getLocation());
        if (distance > ModelsConstants.DISTANCE_THRESHOLD)
            throw new DistantRestaurantException();
    }

    @Override
    public List<RestaurantSummaryDTO> getNearRestaurantsDTO(String username, int startAt) {
        return restaurantService.getNearRestaurantsDTO(
                userService.getUser(username),
                startAt);
    }

    @Override
    public RestaurantWithMenuDTO getNearRestaurantDTOById(String username,
                                                          String id)
            throws DistantRestaurantException,
            RestaurantNotFoundException {

        User user = userService.getUser(username);
        Restaurant restaurant = restaurantService.
                getNearRestaurantById(user, id);

        return restaurantService.makeDTOFromRestaurant(restaurant);
    }

    @Override
    public List<RestaurantSummaryDTO> searchRestaurants(
            String restaurantName, String foodName, String username, int startAt) {

        User user = userService.getUser(username);
        return restaurantService.searchRestaurants(restaurantName, foodName, user, startAt)
                        .stream()
                        .map(restaurantService::getRestaurantDTO)
                        .collect(Collectors.toList());
    }

    @Override
    public void addToCart(String username, String foodId)
            throws FoodNotFoundException,
            ImproperRestaurantException,
            DistantRestaurantException {

        Food food;
        User user = userService.getUser(username);

        try {
            food = foodPartyService.findPartyFoodById(foodId);
        } catch (FoodNotFoundException e) {
            food = foodService.findFoodById(foodId);
        }

        checkRestaurantDistance(user, food.getRestaurant());
        cartService.addToCart(user.getUsername(), food);
    }

    @Override
    public Cart getUserCart(String username) {
        return cartService.getUserCart(username);
    }

    @Override
    public CartDTO getUserCartDTO(String username)
    {
        Cart cart = cartService.getUserCart(username);
        return cartService.makeDTOFromCart(cart);
    }

    @Override
    public void removeFromCart(String username, String foodId)
            throws FoodNotFoundException {

        User user = userService.getUser(username);
        Food food;

        try {
            food = foodPartyService.findPartyFoodById(foodId);
        } catch (FoodNotFoundException e) {
            food = foodService.findFoodById(foodId);
        }

        cartService.removeFromCart(user.getUsername(), food);
    }

    @Override
    public Cart getFinalUserCart(User user)
            throws EmptyCartException,
            NoCreditException {

        Cart userCart = getUserCart(user.getUsername());

        if (userCart.getFoods().isEmpty())
            throw new EmptyCartException();

        int totalPrice = cartService.calculateTotalPrice(userCart);
        if (totalPrice > user.getBalance())
            throw new NoCreditException();

        return userCart;
    }

    private void checkPartyFoodValidity(Food food,
                                        Order order)
            throws PartyFoodSoldOut,
            FoodNotFoundException {

        OrderItem orderItem = orderService.getOrderItem(order, food);
        
        PartyFood partyFood = foodPartyService.findPartyFoodById(
                food.getId());
        Integer count = partyFood.getCount();

        if (orderItem.getCount() > count)
            throw new PartyFoodSoldOut();
    }

    private void checkOrderValidity(Order order)
            throws PartyFoodSoldOut,
            PartyFoodExpired {

        for (OrderItem orderItem : order.getOrderItems()) {
            Food food = orderItem.getFood();

            if (food instanceof PartyFood) {
                try {
                    checkPartyFoodValidity(food, order);
                } catch (FoodNotFoundException e) {
                    throw new PartyFoodExpired();
                }
            }
        }
    }

    private void handleOrderPartyFoods(Order order)
            throws PartyFoodExpired,
            PartyFoodSoldOut,
            FoodNotFoundException {

        checkOrderValidity(order);

        for (OrderItem orderItem : order.getOrderItems()) {
            if (orderItem.getFood() instanceof PartyFood) {
                PartyFood partyFood = foodPartyService.findPartyFoodById(
                        orderItem.getFood().getId());
                foodPartyService.decreasePartyFoodCount(partyFood, orderItem.getCount());
            }
        }
    }

    @Override
    public OrderDTO getOrderDTOById(String username,
                                    String id)
            throws NoSuchOrderException {
        User user = userService.getUser(username);
        Order order = orderService.getOrderById(user, id);
        return getOrderDTO(order);
    }

    private OrderDTO getOrderDTO(Order order) {
        return new OrderDTO(
                order.getId(),
                order.getRestaurant().getName(),
                orderService.getListOfOrderItemDTO(order.getOrderItems()),
                order.getState()
        );
    }

    @Override
    public List<OrderDTO> getOrdersDTO(String username) {
        Set<Order> orders = orderService.getUserOrders(
                userService.getUser(username));
        List<OrderDTO> ordersDTO = new ArrayList<>();

        for (Order order : orders) {
            ordersDTO.add(getOrderDTO(order));
        }
        return ordersDTO;
    }

    private Order createNewOrder(Cart cart,
                                 User user)
            throws FoodNotFoundException,
            PartyFoodExpired,
            PartyFoodSoldOut,
            EmptyCartException {

        Restaurant restaurant = cartService.getRestaurant(cart);

        Order order = new Order(orderService.getOrderItems(
                cart.getFoods()), restaurant, user);
        handleOrderPartyFoods(order);

        int totalPrice = cartService.calculateTotalPrice(cart);
        userService.updateUserBalance(user, -totalPrice);
        cartService.clearCart(user.getUsername());

        return order;
    }

    @Override
    public Order finalizeOrder(String username)
            throws EmptyCartException,
            NoCreditException,
            PartyFoodExpired,
            PartyFoodSoldOut,
            FoodNotFoundException {

        User user = userService.getUser(username);
        Cart userCart = getFinalUserCart(user);
        Order order = createNewOrder(userCart, user);
        orderService.saveOrder(order);
        FindDeliveryScheduler.startScheduler(order);
        return order;
    }

    public void initializeCart(String username, List<CartItemDTO> cartDTO)
            throws ImproperRestaurantException,
            DistantRestaurantException,
            FoodNotFoundException {

        try {
            cartService.clearCart(username);
        } catch (EmptyCartException ignored) { }

        for (CartItemDTO cartItemDTO : cartDTO)
            for (int i = 0; i < cartItemDTO.getCount(); i++)
                addToCart(username, cartItemDTO.getFoodId());
    }

    @Override
    public FoodPartyDTO getFoodPartyDTO() {
        return foodPartyService.getFoodPartyDTO();
    }
}