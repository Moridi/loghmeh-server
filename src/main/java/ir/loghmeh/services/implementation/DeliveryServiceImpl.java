package ir.loghmeh.services.implementation;

import ir.loghmeh.models.delivery.Delivery;
import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.order.DeliveryInfo;
import ir.loghmeh.services.DeliveryService;

import java.io.IOException;
import java.util.List;

public class DeliveryServiceImpl implements DeliveryService {
    private static final DeliveryServiceImpl instance = new DeliveryServiceImpl();

    private DeliveryServiceImpl() {}

    public static DeliveryServiceImpl getInstance() {
        return instance;
    }

    private double calculateDeliveryTime(Delivery delivery, Location userLoc, Location restLoc) {
        double deliveryToRestDistance = delivery.getLocation().calculateDistance(restLoc);
        double restToUserDistance = restLoc.calculateDistance(userLoc);
        return (deliveryToRestDistance + restToUserDistance) / delivery.getVelocity();
    }

    @Override
    public DeliveryInfo findNearestDelivery(Location userLoc, Location restLoc) throws IOException {
        List<Delivery> deliveries = APIServiceImpl.getInstance().fetchDeliveries();
        DeliveryInfo chosen = new DeliveryInfo(null, Double.POSITIVE_INFINITY);

        for (Delivery delivery : deliveries) {
            double time = calculateDeliveryTime(delivery, userLoc, restLoc);
            if (time < chosen.getDeliveryDuration()) {
                chosen.setDelivery(delivery);
                chosen.setDeliveryDuration(time);
            }
        }
        return chosen;
    }
}
