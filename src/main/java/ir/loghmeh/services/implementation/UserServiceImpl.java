package ir.loghmeh.services.implementation;

import ir.loghmeh.models.user.User;
import ir.loghmeh.models.user.dto.UserDTO;
import ir.loghmeh.dataAccess.UserDAO;
import ir.loghmeh.dataAccess.jdbcDrivers.JdbcUserDAO;
import ir.loghmeh.services.UserService;

import ir.loghmeh.utilities.constants.JWTConstants;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class UserServiceImpl implements UserService {
    private static final UserServiceImpl instance = new UserServiceImpl();
    private UserDAO userDAO;

    private UserServiceImpl() {
        userDAO = JdbcUserDAO.getInstance();
    }

    public static UserServiceImpl getInstance() {
        return instance;
    }

    @Override
    public UserDTO getUserDTO(User user) {
        return new UserDTO(user.getUsername(),
                user.getFirstName(), user.getLastName(),
                user.getPhone(), user.getEmail(),
                user.getBalance());
    }

    @Override
    public User getUser(String username) {
        return userDAO.getByName(username);
    }

    @Override
    public Boolean isUserExists(String username) {
        User user = userDAO.getByName(username);

        if (user == null)
            return false;
        return true;
    }

    private String encode(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(JWTConstants.SALT_SEED));
    }

    @Override
    public void registerUser(String firstName, String lastName, String phone,
                             String username, String password) {

        String hashedPassword = encode(password);
        User newUser = new User(firstName, lastName, phone,
                username, hashedPassword);

        userDAO.add(newUser);
    }

    @Override
    public void updateUserBalance(User user, Integer amount) {
        userDAO.updateUserBalance(user, amount);
    }
}
