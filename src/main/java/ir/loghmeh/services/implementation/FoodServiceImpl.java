package ir.loghmeh.services.implementation;

import ir.loghmeh.exceptions.food.DuplicateFoodException;
import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.food.dto.FoodDTO;
import ir.loghmeh.models.food.dto.FoodSummaryDTO;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.restaurant.dto.RestaurantDTO;
import ir.loghmeh.dataAccess.FoodDAO;
import ir.loghmeh.dataAccess.jdbcDrivers.JdbcFoodDAO;
import ir.loghmeh.dataAccess.jdbcDrivers.JdbcRestaurantDAO;
import ir.loghmeh.dataAccess.RestaurantDAO;
import ir.loghmeh.services.FoodService;

import java.util.Set;

public class FoodServiceImpl implements FoodService {
    private static final FoodServiceImpl instance = new FoodServiceImpl();
    private FoodDAO foodDAO;
    private RestaurantDAO restaurantDAO;

    private FoodServiceImpl() {
        foodDAO = JdbcFoodDAO.getInstance();
        restaurantDAO = JdbcRestaurantDAO.getInstance();
    }

    public static FoodServiceImpl getInstance() {
        return instance;
    }

    @Override
    public void add(Food food) {
        foodDAO.add(food);
    }

    @Override
    public void addFood(FoodDTO foodDtoComplete)
            throws DuplicateFoodException,
            RestaurantNotFoundException
    {
        Food food = foodDAO.getFood(foodDtoComplete.getName());
        if (food != null) throw new DuplicateFoodException();

        Food newFood = makeFromFoodDto(foodDtoComplete);
        foodDAO.add(newFood);
    }

    @Override
    public Food findFoodById(String foodId)
            throws FoodNotFoundException {

        Food food = foodDAO.getFood(foodId);
        if (food == null)
            throw new FoodNotFoundException();

        return food;
    }

    @Override
    public Food makeFromFoodDto(FoodDTO foodDtoComplete)
            throws RestaurantNotFoundException {

        Restaurant restaurant = restaurantDAO.getByName(
                foodDtoComplete.getRestaurantName());
        if (restaurant == null) throw new RestaurantNotFoundException();

        return new Food(foodDtoComplete.getName(),
                foodDtoComplete.getDescription(),
                foodDtoComplete.getPopularity(),
                restaurant,
                foodDtoComplete.getPrice(),
                foodDtoComplete.getImage());
    }

    private Boolean isNewFood(String foodName,
                                      Set<Food> foods) {
        for (Food food : foods) {
            if (food.getName().equals(foodName))
                return false;
        }
        return true;
    }

    private Food getFoodFromSummary(FoodSummaryDTO foodSummaryDto,
                                    Restaurant restaurant) {
        return new Food(foodSummaryDto.getName(),
                foodSummaryDto.getDescription(),
                foodSummaryDto.getPopularity(),
                restaurant,
                foodSummaryDto.getPrice(),
                foodSummaryDto.getImage());
    }

    @Override
    public void addRestaurantMenu(RestaurantDTO restaurantDtoComplete,
                                  Restaurant restaurant) {

        Set<Food> foods = foodDAO.getFoodsByRestaurant(restaurant);

        for (FoodSummaryDTO foodSummaryDto : restaurantDtoComplete.getMenu()) {
            if (isNewFood(foodSummaryDto.getName(), foods))
                add(getFoodFromSummary(foodSummaryDto, restaurant));
        }
    }
}
