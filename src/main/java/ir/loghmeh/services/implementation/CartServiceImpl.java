package ir.loghmeh.services.implementation;

import ir.loghmeh.exceptions.cart.EmptyCartException;
import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.restaurant.ImproperRestaurantException;
import ir.loghmeh.models.cart.Cart;
import ir.loghmeh.models.cart.dto.CartDTO;
import ir.loghmeh.models.cart.dto.CartItemDTO;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.dataAccess.CartDAO;
import ir.loghmeh.dataAccess.repositories.CartRepository;
import ir.loghmeh.services.CartService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CartServiceImpl implements CartService {
    private static final CartServiceImpl instance = new CartServiceImpl();
    private CartDAO cartDAO;

    private CartServiceImpl() {
        cartDAO = CartRepository.getInstance();
    }

    public static CartServiceImpl getInstance() {
        return instance;
    }

    @Override
    public void addToCart(String username, Food food)
            throws ImproperRestaurantException {
        for (Food cartFood : getUserCart(username).getFoods()) {
            if (!food.getRestaurant().getName().equals(
                    cartFood.getRestaurant().getName()))
                throw new ImproperRestaurantException();
        }

        cartDAO.addToCart(username, food);
    }

    @Override
    public void clearCart(String username)
            throws EmptyCartException {
        if (cartDAO.getCart(username).getFoods().isEmpty())
            throw new EmptyCartException();
        cartDAO.clearCart(username);
    }

    @Override
    public Cart getUserCart(String username) {
        return cartDAO.getCart(username);
    }

    @Override
    public HashMap<String, Integer> countFoods(List<Food> foods) {
        HashMap<String, Integer> countMap = new HashMap<>();

        for (Food food : foods) {
            if (countMap.get(food.getId()) != null)
                countMap.put(food.getId(), countMap.get(food.getId()) + 1);
            else
                countMap.put(food.getId(), 1);
        }

        return countMap;
    }

    @Override
    public CartDTO makeDTOFromCart(Cart cart) {
        CartDTO cartDto = new CartDTO();

        List<CartItemDTO> items = new ArrayList<>();
        HashMap<String, Integer> map = countFoods(cart.getFoods());

        for (Food food : cart.getFoods()) {
            if (map.get(food.getId()) == null)
                continue;

            Integer count = map.get(food.getId());
            Integer price = count * food.getPrice();
            items.add(new CartItemDTO(count,
                    food.getId(),
                    food.getName(),
                    price, food.getRestaurant().getId()));

            map.remove(food.getId());
        }

        cartDto.setItems(items);
        return cartDto;
    }

    @Override
    public int calculateTotalPrice(Cart cart) {
        int total = 0;
        for (Food food : cart.getFoods())
            total += food.getPrice();
        return total;
    }

    @Override
    public void removeFromCart(String username, Food food)
            throws FoodNotFoundException {
        ArrayList<Food> userCart = (ArrayList<Food>)
                getUserCart(username).getFoods();

        for (Food cartFood : userCart) {
            if (food.getRestaurant().getName().equals(
                    cartFood.getRestaurant().getName())
                    && food.getName().equals(cartFood.getName())) {
                cartDAO.removeFromCart(username, cartFood);
                return;
            }
        }
        throw new FoodNotFoundException();
    }

    @Override
    public Restaurant getRestaurant(Cart cart) {
        if (cart.getFoods().size() == 0) return null;
        return cart.getFoods().get(0).getRestaurant();
    }
}
