package ir.loghmeh.services.implementation;

import ir.loghmeh.exceptions.restaurant.DistantRestaurantException;
import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.food.dto.FoodSummaryDTO;
import ir.loghmeh.models.foodParty.dto.FoodPartyRestaurantDTO;
import ir.loghmeh.models.foodParty.PartyFood;
import ir.loghmeh.models.foodParty.dto.PartyFoodDTO;
import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.restaurant.*;
import ir.loghmeh.models.restaurant.dto.RestaurantDTO;
import ir.loghmeh.models.restaurant.dto.RestaurantSummaryDTO;
import ir.loghmeh.models.restaurant.dto.RestaurantWithMenuDTO;
import ir.loghmeh.models.user.User;
import ir.loghmeh.dataAccess.FoodDAO;
import ir.loghmeh.dataAccess.PartyFoodDAO;
import ir.loghmeh.dataAccess.jdbcDrivers.JdbcFoodDAO;
import ir.loghmeh.dataAccess.jdbcDrivers.JdbcPartyFoodDAO;
import ir.loghmeh.dataAccess.jdbcDrivers.JdbcRestaurantDAO;
import ir.loghmeh.dataAccess.RestaurantDAO;
import ir.loghmeh.services.RestaurantService;
import ir.loghmeh.utilities.constants.ModelsConstants;
import ir.loghmeh.utilities.constants.SchedulersConstants;

import java.util.*;

public class RestaurantServiceImpl implements RestaurantService {
    private FoodDAO foodDAO;
    private PartyFoodDAO partyFoodDAO;
    private RestaurantDAO restaurantDAO;

    private static final RestaurantServiceImpl instance =
            new RestaurantServiceImpl();

    private RestaurantServiceImpl() {
        restaurantDAO = JdbcRestaurantDAO.getInstance();
        foodDAO = JdbcFoodDAO.getInstance();
        partyFoodDAO = JdbcPartyFoodDAO.getInstance();
    }

    public static RestaurantServiceImpl getInstance() {
        return instance;
    }

    @Override
    public void add(Restaurant restaurant) {
        restaurantDAO.add(restaurant);
    }

    @Override
    public void addRestaurant(RestaurantDTO restaurantDtoComplete) {
        if (restaurantDAO.getById(restaurantDtoComplete.getId()) != null)
            return;

        Restaurant restaurant = makeFromRestaurantDto(restaurantDtoComplete);
        restaurantDAO.add(restaurant);
    }

    @Override
    public void addFoodPartyRestaurant(
            FoodPartyRestaurantDTO foodPartyRestaurantDTO) {
        Restaurant restaurant = restaurantDAO.getById(
                foodPartyRestaurantDTO.getId());
        if (restaurant == null) {
            restaurant = new Restaurant(
                    foodPartyRestaurantDTO.getId(),
                    foodPartyRestaurantDTO.getName(),
                    foodPartyRestaurantDTO.getLocation(),
                    foodPartyRestaurantDTO.getLogo());

            restaurantDAO.add(restaurant);
        }

        if (foodPartyRestaurantDTO.getMenu() != null) {
            for (PartyFoodDTO foodDto : foodPartyRestaurantDTO.getMenu()) {
                partyFoodDAO.add(
                        new PartyFood(
                                foodDto.getName(),
                                foodDto.getDescription(),
                                foodDto.getPopularity(),
                                restaurant,
                                foodDto.getPrice(),
                                foodDto.getOldPrice(),
                                foodDto.getCount(),
                                foodDto.getImage()
                        )
                );
            }
        }
    }

    @Override
    public Restaurant makeFromRestaurantDto(
            RestaurantDTO restaurantDtoComplete) {
        return new Restaurant(
                restaurantDtoComplete.getId(),
                restaurantDtoComplete.getName(),
                restaurantDtoComplete.getLocation(),
                restaurantDtoComplete.getLogo()
        );
    }

    @Override
    public Restaurant findRestaurantByName(String name)
            throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantDAO.getByName(name);
        if (restaurant == null)
            throw new RestaurantNotFoundException();

        return restaurant;
    }

    @Override
    public Restaurant findRestaurantById(String id)
            throws RestaurantNotFoundException {
        Restaurant restaurant = restaurantDAO.getById(id);
        if (restaurant == null)
            throw new RestaurantNotFoundException();

        return restaurant;
    }

    @Override
    public Set<Food> getRestaurantMenu(Restaurant restaurant) {
        return foodDAO.getFoodsByRestaurant(restaurant);
    }

    private FoodSummaryDTO getFoodDTO(Food food) {
        return new FoodSummaryDTO(
                food.getName(),
                food.getDescription(),
                food.getPopularity(),
                food.getPrice(),
                food.getImage(),
                food.getId()
        );
    }

    @Override
    public RestaurantSummaryDTO getRestaurantDTO(Restaurant restaurant) {
        RestaurantSummaryDTO restaurantSummaryDto = new RestaurantSummaryDTO();
        restaurantSummaryDto.setId(restaurant.getId());
        restaurantSummaryDto.setName(restaurant.getName());
        restaurantSummaryDto.setLogo(restaurant.getLogo());

        return restaurantSummaryDto;
    }

    @Override
    public RestaurantWithMenuDTO makeDTOFromRestaurant(Restaurant restaurant) {
        RestaurantWithMenuDTO restaurantDto = new RestaurantWithMenuDTO();
        restaurantDto.setId(restaurant.getId());
        restaurantDto.setName(restaurant.getName());
        restaurantDto.setLogo(restaurant.getLogo());

        restaurantDto.setMenu(new ArrayList<>());

        for (Food food : getRestaurantMenu(restaurant)) {
            restaurantDto.getMenu().add(getFoodDTO(food));
        }

        return restaurantDto;
    }

    @Override
    public List<Restaurant> getNearRestaurants(User user, int startAt) {
        return restaurantDAO.getUserNearRestaurants(user, startAt);
    }

    @Override
    public List<RestaurantSummaryDTO> getNearRestaurantsDTO(User user, int startAt) {
        List<Restaurant> restaurants = getNearRestaurants(user, startAt);
        List<RestaurantSummaryDTO> items = new ArrayList<>();

        for (Restaurant restaurant : restaurants) {
            items.add(getRestaurantDTO(restaurant));
        }
        return items;
    }

    @Override
    public Restaurant getNearRestaurantById(User user, String id)
            throws RestaurantNotFoundException, DistantRestaurantException {
        Restaurant restaurant = findRestaurantById(id);

        Location userLocation = user.getLocation();

        if (restaurant.getLocation().
                calculateDistance(userLocation)
                < ModelsConstants.DISTANCE_THRESHOLD)
            return restaurant;

        throw new DistantRestaurantException();
    }

    @Override
    public List<Restaurant> searchRestaurants(String restaurantName, String foodName,
                                              User user, int startAt) {
        return restaurantDAO.searchByNameAndFoodName(restaurantName, foodName, user, startAt);
    }
}
