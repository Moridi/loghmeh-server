package ir.loghmeh.services.implementation;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.loghmeh.models.delivery.Delivery;
import ir.loghmeh.models.foodParty.dto.FoodPartyRestaurantDTO;
import ir.loghmeh.models.restaurant.dto.RestaurantDTO;
import ir.loghmeh.services.APIService;
import ir.loghmeh.services.FoodService;
import ir.loghmeh.services.RestaurantService;
import ir.loghmeh.utilities.http.HttpClient;

import java.io.IOException;
import java.util.List;

public class APIServiceImpl implements APIService {
    private static final APIServiceImpl instance = new APIServiceImpl();

    private final String HOST_ADDR = "http://138.197.181.131:8080";
    private final String RESTAURANTS_PATH = "/restaurants";
    private final String DELIVERIES_PATH = "/deliveries";
    private final String FOOD_PARTY_PATH = "/foodparty";

    private final FoodService foodService;
    private final RestaurantService restaurantService;

    public static APIServiceImpl getInstance() {
        return instance;
    }

    private APIServiceImpl() {
        restaurantService = RestaurantServiceImpl.getInstance();
        foodService = FoodServiceImpl.getInstance();
    }

    @Override
    public void fetchInitialData()
            throws IOException {
        String restaurantsJSON = HttpClient.getInstance().sendGet(
                HOST_ADDR + RESTAURANTS_PATH);
        List<RestaurantDTO> restaurants =
                new ObjectMapper().
                readValue(restaurantsJSON,
                new TypeReference<List<RestaurantDTO>>(){});

        for (RestaurantDTO restaurantDTOComplete : restaurants) {
            restaurantService.addRestaurant(restaurantDTOComplete);
            foodService.addRestaurantMenu(restaurantDTOComplete,
                    restaurantService.makeFromRestaurantDto(
                            restaurantDTOComplete));
        }
    }

    @Override
    public List<Delivery> fetchDeliveries() throws IOException {
        String deliveriesJSON = HttpClient.getInstance().sendGet(
                HOST_ADDR + DELIVERIES_PATH);
        return new ObjectMapper().readValue(
                deliveriesJSON, new TypeReference<List<Delivery>>(){});
    }

    @Override
    public void fetchFoodPartyData() throws IOException {
        String resultJSON = HttpClient.getInstance().sendGet(
                HOST_ADDR + FOOD_PARTY_PATH);
        List<FoodPartyRestaurantDTO> restaurants =
                new ObjectMapper().readValue(
                resultJSON, new TypeReference<List<FoodPartyRestaurantDTO>>(){});

        FoodPartyServiceImpl.getInstance().removeAllPartyFoods();
        for (FoodPartyRestaurantDTO restaurantDTO : restaurants) {
            restaurantService.addFoodPartyRestaurant(restaurantDTO);
        }
    }
}
