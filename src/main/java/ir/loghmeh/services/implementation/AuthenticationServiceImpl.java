package ir.loghmeh.services.implementation;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import io.jsonwebtoken.*;
import ir.loghmeh.exceptions.authentication.InvalidToken;
import ir.loghmeh.services.AuthenticationService;
import ir.loghmeh.utilities.constants.JWTConstants;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;

public class AuthenticationServiceImpl implements AuthenticationService {

    private static AuthenticationServiceImpl instance;

    private AuthenticationServiceImpl() {}

    public static AuthenticationServiceImpl getInstance() {
        if (instance == null)
            instance = new AuthenticationServiceImpl();
        return instance;
    }

    @Override
    public Claims decodeJWT(String token) {

        String base64SecretKey = Base64.getEncoder().
                encodeToString(JWTConstants.SECRET_KEY.getBytes());

        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.
                        parseBase64Binary(base64SecretKey))
                .parseClaimsJws(token).getBody();
        return claims;
    }

    @Override
    public String createJWT(String username) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        String base64SecretKey = Base64.getEncoder().
                encodeToString(JWTConstants.SECRET_KEY.getBytes());

        byte[] apiKeySecretBytes = DatatypeConverter.
                parseBase64Binary(base64SecretKey);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes,
                signatureAlgorithm.getJcaName());

        long expMillis = nowMillis + JWTConstants.LIFE_TIME;
        Date exp = new Date(expMillis);

        JwtBuilder builder = Jwts.builder()
                .setHeaderParam("typ", JWTConstants.TOKEN_TYPE)
                .setId(username)
                .setIssuedAt(now)
                .setIssuer(JWTConstants.TOKEN_ISSUER)
                .setExpiration(exp);

        builder.signWith(signingKey, signatureAlgorithm);

        return builder.compact();
    }

    @Override
    public String decodeGoogleIdToken(String token) throws InvalidToken {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(
                new NetHttpTransport(), new JacksonFactory())
                .setAudience(Collections.singletonList(JWTConstants.WEB_CLIENT_ID))
                .setIssuer("accounts.google.com")
                .build();

        try {
            GoogleIdToken googleIdToken = verifier.verify(token);
            if (googleIdToken == null) throw new InvalidToken();

            GoogleIdToken.Payload payload = googleIdToken.getPayload();
            return payload.getEmail();
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
            throw new InvalidToken();
        }
    }
}