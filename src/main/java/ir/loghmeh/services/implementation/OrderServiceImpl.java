package ir.loghmeh.services.implementation;

import ir.loghmeh.exceptions.order.NoSuchOrderException;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.order.DeliveryInfo;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.OrderItem;
import ir.loghmeh.models.order.dto.OrderItemDTO;
import ir.loghmeh.models.order.OrderState;
import ir.loghmeh.models.user.User;
import ir.loghmeh.dataAccess.OrderDAO;
import ir.loghmeh.dataAccess.jdbcDrivers.JdbcOrderDAO;
import ir.loghmeh.schedulers.delivery.DeliverOrderTimer;
import ir.loghmeh.schedulers.delivery.FindDeliveryScheduler;
import ir.loghmeh.services.OrderService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class OrderServiceImpl implements OrderService {
    private static final OrderServiceImpl instance =
            new OrderServiceImpl();
    private OrderDAO orderDAO;

    private OrderServiceImpl() {
        orderDAO = JdbcOrderDAO.getInstance();
    }

    public static OrderServiceImpl getInstance() {
        return instance;
    }

    @Override
    public void saveOrder(Order order) {
        orderDAO.add(order);
    }

    @Override
    public void updateOrderState(Order order, OrderState state) {
        orderDAO.updateOrderState(order, state);
    }

    @Override
    public Order getOrderById(User user, String orderId)
            throws NoSuchOrderException {
        Set<Order> orders = orderDAO.getUserOrders(user);

        for (Order order : orders)
            if (order.getId().equals(orderId))
                return order;

        throw new NoSuchOrderException();
    }

    @Override
    public List<OrderItemDTO> getListOfOrderItemDTO(List<OrderItem> orderItems) {
        List<OrderItemDTO> orders = new ArrayList<>();

        for (OrderItem orderItem : orderItems) {
            orders.add(new OrderItemDTO(orderItem.getCount(),
                    orderItem.getFood().getName(),
                    orderItem.getFood().getPrice()));
        }

        return orders;
    }

    @Override
    public Set<Order> getUserOrders(User user) {
        return orderDAO.getUserOrders(user);
    }

    @Override
    public void setOrderDelivery(Order order, DeliveryInfo deliveryInfo) {
        orderDAO.updateOrderDelivery(order, deliveryInfo);
    }

    @Override
    public OrderItem getOrderItem(Order order, Food food) {
        List<OrderItem> orderItems = order.getOrderItems();

        for (OrderItem orderItem : orderItems)
            if (orderItem.getFood().getId().equals(food.getId()))
                return orderItem;

        return null;
    }

    private OrderItem getOrderItem(List<OrderItem> orderItems,
                                   Food food) {
        for (OrderItem orderItem : orderItems) {
            if (orderItem.getFood().getId().equals(
                    food.getId()))
                return orderItem;
        }

        return null;
    }

    @Override
    public List<OrderItem> getOrderItems(List<Food> foods) {
        ArrayList<OrderItem> orderItems = new ArrayList<>();

        for (Food food : foods)
        {
            OrderItem orderItem = getOrderItem(orderItems, food);
            if (orderItem == null)
                orderItems.add(new OrderItem(food));
            else
                orderItem.increaseCount();
        }

        return orderItems;
    }

    private void startFindingDeliverySchedulers() {
        Set<Order> findingOrders = orderDAO.getUserOrdersWithState(
                OrderState.FINDING_DELIVERY);

        for (Order order : findingOrders)
            FindDeliveryScheduler.startScheduler(order);
    }

    private void startDeliveryFoundSchedulers() {
        Set<Order> deliveryFoundOrders = orderDAO.getUserOrdersWithState(
                OrderState.DELIVERY_FOUND);

        for (Order order : deliveryFoundOrders) {
            DeliveryInfo deliveryInfo = orderDAO.getDeliveryInfo(order);
            order.setDeliveryInfo(deliveryInfo);
            DeliverOrderTimer.startTimer(order);
        }
    }

    @Override
    public void initializeSchedulers() {
        startFindingDeliverySchedulers();
        startDeliveryFoundSchedulers();
    }
}
