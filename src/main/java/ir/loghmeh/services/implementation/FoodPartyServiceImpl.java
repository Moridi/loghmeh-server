package ir.loghmeh.services.implementation;

import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.foodParty.PartyFoodSoldOut;
import ir.loghmeh.models.foodParty.PartyFood;
import ir.loghmeh.models.foodParty.dto.PartyFoodDTO;
import ir.loghmeh.models.foodParty.dto.FoodPartyDTO;
import ir.loghmeh.dataAccess.PartyFoodDAO;
import ir.loghmeh.dataAccess.jdbcDrivers.JdbcPartyFoodDAO;
import ir.loghmeh.schedulers.foodParty.FetchFoodPartyDataScheduler;
import ir.loghmeh.services.FoodPartyService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FoodPartyServiceImpl implements FoodPartyService {
    private PartyFoodDAO partyFoodDAO;
    private static final FoodPartyServiceImpl instance =
            new FoodPartyServiceImpl();

    private FoodPartyServiceImpl() {
        this.partyFoodDAO = JdbcPartyFoodDAO.getInstance();
    }

    public static FoodPartyServiceImpl getInstance() {
        return instance;
    }

    @Override
    public void removeAllPartyFoods() {
        partyFoodDAO.expire();
    }

    private PartyFoodDTO getPartyFood(PartyFood partyFood) {
        return new PartyFoodDTO(
                partyFood.getName(),
                partyFood.getDescription(),

                partyFood.getPopularity(),
                partyFood.getRestaurant().getName(),

                partyFood.getPrice(),
                partyFood.getImage(),

                partyFood.getOldPrice(),
                partyFood.getCount(),
                partyFood.getRestaurant().getId(),
                partyFood.getId()
        );
    }

    @Override
    public List<PartyFoodDTO> getPartyFoodsDTO() {
        Set<PartyFood> foods = partyFoodDAO.getAll();
        List<PartyFoodDTO> foodsDTO = new ArrayList<>();

        for (PartyFood partyFood : foods) {
            foodsDTO.add(getPartyFood(partyFood));
        }

        return foodsDTO;
    }

    @Override
    public FoodPartyDTO getFoodPartyDTO() {
        FoodPartyDTO foodPartyDTO = new FoodPartyDTO();

        List<PartyFoodDTO> foods = getPartyFoodsDTO();

        Integer remainingTime = FetchFoodPartyDataScheduler.
                getInstance().getRemainingTime();

        foodPartyDTO.setFoods(foods);
        foodPartyDTO.setRemainingTime(remainingTime);

        return foodPartyDTO;
    }


    @Override
    public PartyFood findPartyFoodById(String foodId)
            throws FoodNotFoundException {

        PartyFood food = partyFoodDAO.getByFoodId(foodId);
        if (food == null)
            throw new FoodNotFoundException();
        return food;
    }

    @Override
    public void decreasePartyFoodCount(PartyFood food, int amount)
            throws PartyFoodSoldOut {
        if (food.getCount() < amount)
            throw new PartyFoodSoldOut();
        partyFoodDAO.decreaseFoodCount(food, amount);
    }
}
