package ir.loghmeh.services;

import io.jsonwebtoken.Claims;
import ir.loghmeh.exceptions.authentication.InvalidToken;

public interface AuthenticationService {
    Claims decodeJWT(String token);

    String createJWT(String username);

    String decodeGoogleIdToken(String token) throws InvalidToken;
}
