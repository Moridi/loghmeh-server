package ir.loghmeh.services;

import ir.loghmeh.exceptions.restaurant.DistantRestaurantException;
import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.foodParty.dto.FoodPartyRestaurantDTO;
import ir.loghmeh.models.restaurant.*;
import ir.loghmeh.models.restaurant.dto.RestaurantDTO;
import ir.loghmeh.models.restaurant.dto.RestaurantSummaryDTO;
import ir.loghmeh.models.restaurant.dto.RestaurantWithMenuDTO;
import ir.loghmeh.models.user.User;

import java.util.List;
import java.util.Set;

public interface RestaurantService {
    void add(Restaurant restaurant);

    void addRestaurant(RestaurantDTO restaurantDtoComplete);

    Restaurant makeFromRestaurantDto(RestaurantDTO restaurantDtoComplete);

    Restaurant findRestaurantByName(String name)
            throws RestaurantNotFoundException;

    Restaurant findRestaurantById(String id)
            throws RestaurantNotFoundException;

    Set<Food> getRestaurantMenu(Restaurant restaurant);

    RestaurantWithMenuDTO makeDTOFromRestaurant(Restaurant restaurant);

    List<Restaurant> getNearRestaurants(User user, int startAt);

    List<RestaurantSummaryDTO> getNearRestaurantsDTO(User user, int startAt);

    Restaurant getNearRestaurantById(User user,
                                     String id)
            throws RestaurantNotFoundException,
            DistantRestaurantException;

    RestaurantSummaryDTO getRestaurantDTO(Restaurant restaurant);

    void addFoodPartyRestaurant(FoodPartyRestaurantDTO foodPartyRestaurantDTO);

    List<Restaurant> searchRestaurants(String restaurantName, String foodName,
                                       User user, int startAt);
}
