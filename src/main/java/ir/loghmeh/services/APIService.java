package ir.loghmeh.services;

import ir.loghmeh.exceptions.restaurant.DuplicateRestaurantException;
import ir.loghmeh.models.delivery.Delivery;

import java.io.IOException;
import java.util.List;

public interface APIService {
    void fetchInitialData()
            throws IOException;

    List<Delivery> fetchDeliveries()
            throws IOException;

    void fetchFoodPartyData()
            throws IOException;
}
