package ir.loghmeh.services;

import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.order.DeliveryInfo;

import java.io.IOException;

public interface DeliveryService {
    DeliveryInfo findNearestDelivery(Location userLoc,
                                     Location restLoc)
            throws IOException;
}
