package ir.loghmeh.services;

import ir.loghmeh.models.user.User;
import ir.loghmeh.models.user.dto.UserDTO;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public interface UserService {

    User getUser(String username);

    Boolean isUserExists(String username);

    void registerUser(String firstName,
                      String lastName,
                      String phone,
                      String username,
                      String password);

    UserDTO getUserDTO(User user);

    void updateUserBalance(User user, Integer amount);
}
