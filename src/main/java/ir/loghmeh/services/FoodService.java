package ir.loghmeh.services;

import ir.loghmeh.exceptions.food.DuplicateFoodException;
import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.food.dto.FoodDTO;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.restaurant.dto.RestaurantDTO;

public interface FoodService {
    void add(Food food);

    void addFood(FoodDTO foodDtoComplete)
            throws DuplicateFoodException,
            RestaurantNotFoundException;

    void addRestaurantMenu(RestaurantDTO restaurantDtoComplete,
                           Restaurant restaurant);
    Food findFoodById(String foodId)
            throws FoodNotFoundException;

    Food makeFromFoodDto(FoodDTO foodDtoComplete)
            throws RestaurantNotFoundException;
}
