package ir.loghmeh.services;

import ir.loghmeh.exceptions.cart.EmptyCartException;
import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.restaurant.ImproperRestaurantException;
import ir.loghmeh.models.cart.Cart;
import ir.loghmeh.models.cart.dto.CartDTO;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.restaurant.Restaurant;

import java.util.HashMap;
import java.util.List;

public interface CartService {

    void addToCart(String username,
                   Food food)
            throws ImproperRestaurantException;

    void clearCart(String username)
            throws EmptyCartException;

    Cart getUserCart(String username);

    HashMap<String, Integer> countFoods(List<Food> foods);

    CartDTO makeDTOFromCart(Cart cart);

    int calculateTotalPrice(Cart cart);

    void removeFromCart(String username,
                        Food food)
            throws FoodNotFoundException;

    Restaurant getRestaurant(Cart cart);
}