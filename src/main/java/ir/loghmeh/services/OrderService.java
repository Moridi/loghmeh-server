package ir.loghmeh.services;

import ir.loghmeh.exceptions.order.NoSuchOrderException;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.order.DeliveryInfo;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.OrderItem;
import ir.loghmeh.models.order.dto.OrderItemDTO;
import ir.loghmeh.models.order.OrderState;
import ir.loghmeh.models.user.User;

import java.util.List;
import java.util.Set;

public interface OrderService {
    void saveOrder(Order order);

    void updateOrderState(Order order, OrderState state);

    Order getOrderById(User user, String orderId)
            throws NoSuchOrderException;

    List<OrderItemDTO> getListOfOrderItemDTO(List<OrderItem> orderItems);

    Set<Order> getUserOrders(User user);

    void setOrderDelivery(Order order, DeliveryInfo deliveryInfo);

    OrderItem getOrderItem(Order order, Food food);

    List<OrderItem> getOrderItems(List<Food> foods);

    void initializeSchedulers();
}