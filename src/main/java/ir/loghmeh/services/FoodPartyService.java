package ir.loghmeh.services;

import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.foodParty.PartyFoodSoldOut;
import ir.loghmeh.models.foodParty.PartyFood;
import ir.loghmeh.models.foodParty.dto.PartyFoodDTO;
import ir.loghmeh.models.foodParty.dto.FoodPartyDTO;

import java.util.List;

public interface FoodPartyService {
    List<PartyFoodDTO> getPartyFoodsDTO();

    void removeAllPartyFoods();

    PartyFood findPartyFoodById(String foodId)
            throws FoodNotFoundException;

    FoodPartyDTO getFoodPartyDTO();

    void decreasePartyFoodCount(PartyFood food, int amount)
            throws PartyFoodSoldOut;
}
