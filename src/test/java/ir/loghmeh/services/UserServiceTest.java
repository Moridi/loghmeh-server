package ir.loghmeh.services;

import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.user.User;
import ir.loghmeh.dataAccess.UserDAO;
import ir.loghmeh.services.implementation.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

public class UserServiceTest {

    @Captor
    ArgumentCaptor<Integer> integerArgumentCaptor;

    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    @Mock
    UserDAO userDAO;

    @InjectMocks
    UserServiceImpl userServiceImpl;

    private User user;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        user = new User(
                "ghanbari@gmail.com",
                "mohammad",
                "ghanbari",
                "+989033683186",
                "mail.mmdghanbari@gmail.com",
                new Location(0, 0));
    }

    @Test
    public void someAmountIsGiven_UpdateUserBalanceIsInvoked_ValidArgumentExpected() {
        Integer amount = 13;

        userServiceImpl.updateUserBalance(user, amount);

        verify(userDAO).updateUserBalance(userArgumentCaptor.capture(),
                integerArgumentCaptor.capture());

        assertEquals(user, userArgumentCaptor.getValue());
        assertEquals(amount, integerArgumentCaptor.getValue());
    }
}
