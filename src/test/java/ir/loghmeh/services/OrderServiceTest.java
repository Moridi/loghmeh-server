package ir.loghmeh.services;

import ir.loghmeh.exceptions.order.NoSuchOrderException;
import ir.loghmeh.models.order.Order;
import ir.loghmeh.models.order.OrderItem;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.user.User;
import ir.loghmeh.dataAccess.OrderDAO;
import ir.loghmeh.services.implementation.OrderServiceImpl;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class OrderServiceTest {

    private static Order firstOrder;
    private static Order secondOrder;
    private static Set<Order> orders;

    @Mock
    static List<OrderItem> orderItems;
    @Mock
    static Restaurant restaurant;
    @Mock
    static User user;

    @Mock
    OrderDAO orderDAO;
    @InjectMocks
    OrderServiceImpl orderServiceImpl;

    @BeforeClass
    public static void init() {
        firstOrder = new Order(orderItems, restaurant, user);
        secondOrder = new Order(orderItems, restaurant, user);

        orders = new HashSet<Order>();

        orders.add(firstOrder);
        orders.add(secondOrder);
    }

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
    }

}