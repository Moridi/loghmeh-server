package ir.loghmeh.services;

import ir.loghmeh.exceptions.food.DuplicateFoodException;
import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.food.dto.FoodDTO;
import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.dataAccess.FoodDAO;
import ir.loghmeh.dataAccess.RestaurantDAO;
import ir.loghmeh.services.implementation.FoodServiceImpl;
import org.junit.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class FoodServiceImplTest {
    private Restaurant restaurant;
    private FoodDTO foodDtoComplete;
    private FoodDTO invalidFoodDTOComplete;
    private Food food;

    @Mock
    private RestaurantDAO restaurantDAO;
    @Mock
    private FoodDAO foodDAO;
    @InjectMocks
    private FoodServiceImpl foodServiceImpl;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        foodDtoComplete = new FoodDTO("Ghormeh", "Delicious",
                0.99, "Shandiz", 50000, "", "0");
        invalidFoodDTOComplete = new FoodDTO("Ghormeh", "Delicious", 0.99,
                "InvalidRestaurant", 50000, "", "0");

        restaurant = new Restaurant("123", "Shandiz", new Location(1, 2), "");
        food = new Food("Ghormeh", "Delicious",
                0.99, restaurant, 50000, "");
    }

    void assertEqualFoodAndDto(FoodDTO foodDtoComplete, Food food) {
        assertTrue(foodDtoComplete.getName().equals(food.getName()));
        assertTrue(foodDtoComplete.getDescription().equals(food.getDescription()));
        assertEquals(foodDtoComplete.getPopularity(), food.getPopularity());
        assertTrue(foodDtoComplete.getRestaurantName().equals(food.getRestaurant().getName()));
    }

    @Test
    public void completeFoodDtoIsGiven_InvokeMakeFromFoodDto_ReturnSameFood()
            throws RestaurantNotFoundException {

        when(restaurantDAO.getByName(anyString())).thenReturn(restaurant);
        Food food = foodServiceImpl.makeFromFoodDto(foodDtoComplete);
        assertEqualFoodAndDto(foodDtoComplete, food);
    }

    @Test (expected = RestaurantNotFoundException.class)
    public void invalidFoodDtoIsGiven_InvokeMakeFromFoodDto_ReturnNullExpected()
            throws RestaurantNotFoundException {
        Food food = foodServiceImpl.makeFromFoodDto(invalidFoodDTOComplete);
        assertNull(food);
    }

    @Test (expected = RestaurantNotFoundException.class)
    public void restaurantNotFoundIsGiven_InvokeAddFood_RestaurantNotFoundExceptionExpected()
            throws DuplicateFoodException, RestaurantNotFoundException{
        FoodDTO restaurantNotFound = new FoodDTO("Ghormeh", "Delicious",
                0.99, "Somewhere Else", 50000, "", "0");
        foodServiceImpl.addFood(restaurantNotFound);
    }

    @Test (expected = RestaurantNotFoundException.class)
    public void restaurantNameNotFoundIsGiven_InvokeMakeFromFoodDto_RestaurantNotFoundExceptionExpected()
            throws RestaurantNotFoundException {
        foodServiceImpl.makeFromFoodDto(invalidFoodDTOComplete);
    }
}