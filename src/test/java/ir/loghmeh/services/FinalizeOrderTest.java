package ir.loghmeh.services;

import ir.loghmeh.exceptions.cart.EmptyCartException;
import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.exceptions.foodParty.PartyFoodExpired;
import ir.loghmeh.exceptions.foodParty.PartyFoodSoldOut;
import ir.loghmeh.exceptions.user.NoCreditException;
import ir.loghmeh.models.cart.Cart;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.user.User;
import ir.loghmeh.services.implementation.ApplicationServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

public class FinalizeOrderTest {
    private ArrayList<Food> foods;
    private HashSet<Restaurant> restaurants;

    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    @Captor
    ArgumentCaptor<Integer> integerArgumentCaptor;

    @Mock
    private User user;

    @Mock
    private Cart cart;

    @Mock
    private RestaurantService restaurantService;

    @Mock
    private CartService cartService;

    @Mock
    private UserService userService;

    @InjectMocks
    private ApplicationServiceImpl applicationServiceImpl;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        Restaurant akbar  = new Restaurant("1", "AkbarJoojeh", new Location(1, 2), "");
        Restaurant god  = new Restaurant("2", "Godfather", new Location(3, 2), "");
        Restaurant shandiz  = new Restaurant("3", "Shandiz", new Location(100, 200), "");
        Restaurant naab  = new Restaurant("4", "Naab", new Location(100, 200), "");
        Restaurant ferri  = new Restaurant("5", "FerriKasif", new Location(1000, 2000), "");

        foods = new ArrayList<Food>() {
        {
            add(new Food("Ghormeh", "Delicious", 0.5, akbar, 10000, ""));
            add(new Food("Gheimeh", "Delicious", 0.6, akbar, 200000, ""));

            add(new Food("Pizza", "Delicious", 0.5, god, 30000, ""));
            add(new Food("Burger", "Delicious", 0.6, god, 400000, ""));

            add(new Food("Kalapch", "Delicious", 0.9, shandiz, 5000, ""));
            add(new Food("Pasta", "Delicious", 0.8, shandiz, 50000, ""));

            add(new Food("Chicken", "Delicious", 0.1, naab, 8000, ""));
            add(new Food("KooKoo", "Delicious", 0.2, naab, 80000, ""));

            add(new Food("Kotlet", "Delicious", 0.9, ferri, 1000000, ""));
            add(new Food("Abgoosht", "Delicious", 0.9, ferri, 30000, ""));
        }};

        when(cartService.getUserCart(any())).thenReturn(cart);
        when(cart.getFoods()).thenReturn(foods);
        when(userService.getUser(anyString())).thenReturn(user);
    }

    void assertEqualFood(Food firstFood, Food secondFood) {
        assertTrue(firstFood.getName().equals(secondFood.getName()));
        assertTrue(firstFood.getDescription().equals(secondFood.getDescription()));
        assertEquals(firstFood.getPopularity(), secondFood.getPopularity());
    }

    @Test (expected = NoCreditException.class)
    public void noCreditedUserIsGiven_FinalizeOrderIsInvoked_NoCreditExceptionExpected()
            throws EmptyCartException, NoCreditException,
            PartyFoodExpired, PartyFoodSoldOut, FoodNotFoundException {

        // @FIXME: Get username as an attribute
        String username = new String("ghanbari@gmail.com");

        when(cartService.calculateTotalPrice(any())).thenReturn(100);
        when(user.getBalance()).thenReturn(5);
        applicationServiceImpl.finalizeOrder(username);
    }
}