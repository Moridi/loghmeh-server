package ir.loghmeh.services;

import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.food.dto.FoodSummaryDTO;
import ir.loghmeh.models.food.dto.FoodDTO;
import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.restaurant.dto.RestaurantDTO;
import ir.loghmeh.models.user.User;
import ir.loghmeh.dataAccess.FoodDAO;
import ir.loghmeh.dataAccess.RestaurantDAO;
import ir.loghmeh.services.implementation.RestaurantServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class RestaurantServiceImplTest {
    private Restaurant restaurant;
    private RestaurantDTO restaurantDtoComplete;
    private FoodSummaryDTO foodSummaryDtoComplete;
    private Food food;
    private ArrayList<FoodSummaryDTO> dtoMenu;
    private HashSet<Food> menu;

    @Mock
    private RestaurantDAO restaurantDAO;
    @Mock
    private FoodDAO foodDAO;
    @InjectMocks
    private RestaurantServiceImpl restaurantServiceImpl;

    private HashSet<Restaurant> restaurants;

    private User user;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        restaurants = new HashSet<Restaurant>();

        foodSummaryDtoComplete = new FoodDTO("Ghormeh", "Delicious", 0.99, "Shandiz", 50000, "", "0");
        food = new Food("Ghormeh", "Delicious", 0.99, null, 50000, "");

        dtoMenu = new ArrayList<FoodSummaryDTO>();
        dtoMenu.add(foodSummaryDtoComplete);
        restaurantDtoComplete = new RestaurantDTO("123", "Shandiz", new Location(1, 2), dtoMenu, "");

        menu = new HashSet<>();
        menu.add(food);
        restaurant = new Restaurant("456", "Shandiz", new Location(1, 2), "");
        food.setRestaurant(restaurant);

        restaurants.add(restaurant);
        user = new User(
                "ghanbari@gmail.com",
                "mohammad",
                "ghanbari",
                "+989033683186",
                "mail.mmdghanbari@gmail.com",
                new Location(0, 0));
    }

    void assertEqualFood(Food firstFood, Food secondFood) {
        assertEquals(firstFood.getName(), secondFood.getName());
        assertEquals(firstFood.getDescription(), secondFood.getDescription());
        assertEquals(firstFood.getPopularity(), secondFood.getPopularity());
    }

    void assertEqualRestaurantAndDto(RestaurantDTO restaurantDtoComplete, Restaurant restaurant) {
        assertEquals(restaurant.getName(), restaurantDtoComplete.getName());
    }

    void assertEqualRestaurant(Restaurant firstRestaurant, Restaurant secondRestaurant) {
        assertEquals(firstRestaurant.getName(), secondRestaurant.getName());
        assertEquals(firstRestaurant.getLocation().getX(), secondRestaurant.getLocation().getX());
        assertEquals(firstRestaurant.getLocation().getY(), secondRestaurant.getLocation().getY());
    }

    void assertValidMenu(Set<Food> firstMenu, Set<Food> secondMenu) {
        Iterator<Food> firstIterator = firstMenu.iterator();
        Iterator<Food> secondIterator = secondMenu.iterator();

        while(firstIterator.hasNext() && secondIterator.hasNext())
            assertEqualFood(firstIterator.next(), secondIterator.next());
    }

    @Test
    public void completeRestaurantDtoIsGiven_InvokeMakeFromRestaurantDto_ReturnSameRestaurantExpected() {
        Restaurant restaurant = restaurantServiceImpl.makeFromRestaurantDto(restaurantDtoComplete);
        assertEqualRestaurantAndDto(restaurantDtoComplete, restaurant);
    }

    @Test
    public void validRestaurantNameIsGiven_InvokeFindRestaurantByName_ReturnSameRestaurantExpected()
            throws RestaurantNotFoundException {

        when(restaurantDAO.getByName(restaurant.getName())).thenReturn(restaurant);

        Restaurant returnedRestaurant = restaurantServiceImpl.findRestaurantByName(restaurant.getName());
        assertEqualRestaurant(returnedRestaurant, restaurant);
    }
}