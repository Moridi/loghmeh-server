package ir.loghmeh.services;

import ir.loghmeh.exceptions.restaurant.RestaurantNotFoundException;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.user.User;
import ir.loghmeh.services.implementation.ApplicationServiceImpl;
import org.junit.*;
import org.mockito.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;

public class ApplicationServiceImplTest {
    @Mock
    private FoodService foodService;

    @Captor
    ArgumentCaptor<String> stringCaptor;

    @Captor
    ArgumentCaptor<String> anotherStringCaptor;

    @Captor
    ArgumentCaptor<String> userCaptor;

    @Captor
    ArgumentCaptor<Food> foodCaptor;

    @Mock
    private RestaurantService restaurantService;

    @Mock
    private CartService cartService;

    @Mock
    private UserService userService;

    @InjectMocks
    private ApplicationServiceImpl applicationServiceImpl;

    private User user;
    private Food food;
    private Restaurant restaurant;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        user = new User(
                "ghanbari@gmail.com",
                "mohammad",
                "ghanbari",
                "+989033683186",
                "mail.mmdghanbari@gmail.com",
                new Location(0, 0));
        food = new Food("Ghormeh", "Delicious", 0.99, null, 50000, "");

        restaurant = new Restaurant("456", "Shandiz", new Location(1, 2), "");
    }

    void assertEqualFood(Food firstFood, Food secondFood) {
        assertEquals(firstFood.getName(), secondFood.getName());
        assertEquals(firstFood.getDescription(), secondFood.getDescription());
        assertEquals(firstFood.getPopularity(), secondFood.getPopularity());
    }

    void assertEqualRestaurant(Restaurant firstRestaurant, Restaurant secondRestaurant) {
        assertEquals(firstRestaurant.getName(), secondRestaurant.getName());
        assertEquals(firstRestaurant.getLocation().getX(), secondRestaurant.getLocation().getX());
        assertEquals(firstRestaurant.getLocation().getY(), secondRestaurant.getLocation().getY());
    }

    @Test
    public void validRestaurantNameIsGiven_InvokeGetRestaurantByName_ValidRestaurantExpected()
            throws RestaurantNotFoundException {

        String expectedRestaurantName = "Shandiz";
        applicationServiceImpl.getRestaurantByName(expectedRestaurantName);
        verify(restaurantService).findRestaurantByName(stringCaptor.capture());

        assertEquals(expectedRestaurantName, stringCaptor.getValue());
    }

}