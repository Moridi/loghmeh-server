package ir.loghmeh.services;

import ir.loghmeh.exceptions.cart.EmptyCartException;
import ir.loghmeh.exceptions.food.FoodNotFoundException;
import ir.loghmeh.models.cart.Cart;
import ir.loghmeh.models.food.Food;
import ir.loghmeh.models.location.Location;
import ir.loghmeh.models.restaurant.Restaurant;
import ir.loghmeh.models.restaurant.dto.RestaurantDTO;
import ir.loghmeh.models.user.User;
import ir.loghmeh.dataAccess.CartDAO;
import ir.loghmeh.services.implementation.CartServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CartServiceImplTest {
    private Restaurant restaurant;
    private Restaurant invalidRestaurant;
    private Food food;
    private HashSet<Food> menu;
    private User user;
    private Food invalidFood;

    @Mock
    private Cart cart;

    @Captor
    ArgumentCaptor<String> userCaptor;

    @Captor
    ArgumentCaptor<Food> foodCaptor;

    @Mock
    private CartDAO cartDAO;

    @InjectMocks
    private CartServiceImpl cartServiceImpl;


    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        food = new Food("Ghormeh", "Delicious", 0.99, null, 50000, "");
        invalidFood = new Food("Gheimeh", "Delicious", 0.99, null, 50000, "");

        menu = new HashSet<Food>();
        menu.add(food);

        restaurant = new Restaurant("123","Shandiz", new Location(1, 2), "");
        invalidRestaurant = new Restaurant("456", "Naab", new Location(1, 2), "");

        invalidFood.setRestaurant(invalidRestaurant);
        food.setRestaurant(restaurant);

        user = new User(
                "ghanbari@gmail.com",
                "mohammad",
                "ghanbari",
                "+989033683186",
                "mail.mmdghanbari@gmail.com",
                new Location(0, 0));
    }

    void assertEqualFood(Food firstFood, Food secondFood) {
        assertEquals(firstFood.getName(), secondFood.getName());
        assertEquals(firstFood.getDescription(), secondFood.getDescription());
        assertEquals(firstFood.getPopularity(), secondFood.getPopularity());
    }

    void assertEqualRestaurantAndDto(RestaurantDTO restaurantDtoComplete, Restaurant restaurant) {
        assertEquals(restaurant.getName(), restaurantDtoComplete.getName());
        assertEquals(restaurant.getLocation().getX(), restaurantDtoComplete.getLocation().getX());
        assertEquals(restaurant.getLocation().getY(), restaurantDtoComplete.getLocation().getY());
    }

    void assertEqualRestaurant(Restaurant firstRestaurant, Restaurant secondRestaurant) {
        assertEquals(firstRestaurant.getName(), secondRestaurant.getName());
        assertEquals(firstRestaurant.getLocation().getX(), secondRestaurant.getLocation().getX());
        assertEquals(firstRestaurant.getLocation().getY(), secondRestaurant.getLocation().getY());
    }

    void assertValidMenu(Set<Food> firstMenu, Set<Food> secondMenu) {
        Iterator<Food> firstIterator = firstMenu.iterator();
        Iterator<Food> secondIterator = secondMenu.iterator();

        while(firstIterator.hasNext() && secondIterator.hasNext())
            assertEqualFood(firstIterator.next(), secondIterator.next());
    }

    @Test (expected=EmptyCartException.class)
    public void validUserIsGiven_InvokeEmptyCart_EmptyCartExceptionExpected()
            throws EmptyCartException {

        when(cartDAO.getCart(user.getUsername())).thenReturn(cart);
        when(cart.getFoods()).thenReturn(new ArrayList<Food>());

        cartServiceImpl.clearCart(user.getUsername());
    }

    @Test
    public void anItemIsAdded_InvokeGetUserCart_SameFoodExpected()
            throws EmptyCartException{

        cartServiceImpl.getUserCart(user.getUsername());
        verify(cartDAO).getCart(userCaptor.capture());

        assertEquals(user.getUsername(), userCaptor.getValue());
    }

    @Test (expected=FoodNotFoundException.class)
    public void emptyCartIsGiven_InvokeRemoveFromCart_FoodNotFoundExceptionExpected()
            throws FoodNotFoundException {

        when(cartDAO.getCart(user.getUsername())).thenReturn(cart);
        when(cart.getFoods()).thenReturn(new ArrayList<Food>());

        cartServiceImpl.removeFromCart(user.getUsername(), food);
    }

    @Test
    public void validFoodIsGiven_InvokeRemoveFromCart_ValidArgumentsExpected()
            throws FoodNotFoundException {

        ArrayList<Food> sampleFoods = new ArrayList<Food>() {{ add(food); }};

        when(cartDAO.getCart(user.getUsername())).thenReturn(cart);
        when(cart.getFoods()).thenReturn(sampleFoods);

        cartServiceImpl.removeFromCart(user.getUsername(), food);

        verify(cartDAO).removeFromCart(userCaptor.capture(), foodCaptor.capture());

        assertEquals(user.getUsername(), userCaptor.getValue());
        assertEquals(food, foodCaptor.getValue());
    }
}