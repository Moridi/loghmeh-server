FROM maven:3.6.0-jdk-8 AS build_dir
WORKDIR /tmp/
COPY . /tmp

# build all dependencies for offline use
RUN mvn dependency:go-offline -B
RUN mvn package

FROM tomcat:9.0
COPY --from=build_dir /tmp/target/loghmeh*.war $CATALINA_HOME/webapps/ROOT.war

EXPOSE 8080
CMD ["catalina.sh", "run"]
