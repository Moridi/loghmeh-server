# Loghmeh Back-end

Loghmeh is an educational purpose project of the Internet Engineering Course at
the University of Tehran which was held in Spring 2020 by 
Dr. Ehsan Khames Panah's instruction.

## Getting Started

These instructions will get you a copy of the project up and running on 
your local machine for development and testing purposes.

### Docker Pull

Docker is a platform for developers and sysadmins to build, run, and share 
applications with containers. You can simply pull the latest version
of this project from our docker hub to use its APIs.

```
docker pull s3reuis/loghmeh-back-end:latest
```

Note that this image will work with the MySQL database that should
be available as a service with the name of `mysql`.

### Development

This project is written in Java programming language and use
**Apache Tomcat 9.0** provides a "pure Java" HTTP web server environment.
This server will communicate with **MySQL** database as a service in the same
cluster.

Also **REST APIs** are available which can be used in front-end.

#### Clone

First of all the gitlab repositroy should be cloned into your system.
```
git clone https://gitlab.com/Moridi/loghmeh-server.git
```

#### Running the tests

Before building the project with docker, you can test the services
validity via following command in the root directory:
```
mvn test
```
Apache Maven is a software project management and comprehension tool.
Maven can manage a project's build, reporting and documentation
from a central piece of information.

#### Docker Build and Run
After applying your changes you can build the project using the
Dockerfile which is available at the root directory of the project.

```
docker build -t loghmeh-back-end:latest .
docker run loghmeh-back-end:latest
```

## Contributing

Please read 
[CONTRIBUTING.md](https://gitlab.com/Moridi/loghmeh-server/-/blob/master/CONTRIBUTING.md) 
for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Mohammad Moridi** - [Gitlab Profile](https://gitlab.com/Moridi)

See also the list of 
[contributors](https://gitlab.com/Moridi/loghmeh-server/-/graphs/master) 
who participated in this project.


## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
